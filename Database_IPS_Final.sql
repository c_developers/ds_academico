drop database if EXISTS DataBase_DS; 
create database DataBase_DS; 
use DataBase_DS; 

CREATE TABLE Alumnos(
NIE int(11) unsigned primary key,
PNombre varchar(50) not null,
SNombre varchar(50) not null,
Papellido varchar(50) not null,
Sapellido varchar(50) not null,
idDepartamento int unsigned not null default 1,
idMunicipio int unsigned not null default 1,
zonaUR enum('URBANA','RURAL'),
direccion varchar(100), 
calle enum('Pavimentada','Carretera','Rural','Otra'),
fechNacimiento date,
alergias_enfermedades text ,
estatus bool not null,
estatus_descripcion varchar(20) default '', 
-- idBachillerato int unsigned not null,
anioInicio date, 
anioGraducion date
)Engine=INNODB; 

create table Encargados( 
idEncargado int unsigned primary key auto_increment,
nombres_encargado varchar(100) not null, 
apellidos_encargado varchar(100) not null,
telefono varchar(9) , 
DUI varchar (10) not null, 
trabajo varchar(100) not null
); 

create table departamentos(
idDepartamento int unsigned primary key auto_increment,
departamento varchar(50) not null default 'SONSONATE'
)engine=innodb;

create table municipios(
idMunicipio int unsigned primary key auto_increment,
municipio varchar(60) not null default 'SONSONATE',
idDepartamento int unsigned 
)engine=innodb;

CREATE TABLE Docentes(
idDocente int unsigned primary key auto_increment,
NIDocente int unsigned not null, -- Considerar si debemos ponerlo, o el idDocente ocupar el NUID 
NUP varchar(20) , -- AFP de maestro.  
nombre varchar(50) not null,
apellido varchar(50) not null,
DUI varchar(10) default 'xxxxxxxx-x',
NIT varchar(17) default null,
telefono char(8) not null, 
direccion varchar(60),
laborando bool not null, 
idTitulo int unsigned not null
)Engine=INNODB; 

CREATE TABLE Titulos( -- De los docentes 
 idTitulo int unsigned primary key auto_increment, 
nombreCarrera varchar(100) not null 
)Engine=INNODB; 


CREATE TABLE Bachilleratos(
idBachillerato int unsigned primary key auto_increment,
nombreResultante varchar(200) ,
anioLectivo ENUM('1°','2°','3°'),
TipoBachillerato ENUM('GENERAL','TÉCNICO VOCACIONAL COMERCIAL'), 
idEspecialidadBach int unsigned not null
); 


CREATE TABLE EspecialidadBachillerato(
idEspecialidadBach int unsigned primary key auto_increment,
NombreEsp varchar(100) not null  
)Engine=INNODB;


CREATE TABLE Planificacion(
idPlanificacion int unsigned primary key auto_increment,
nombreGrupo varchar(200),
idbachillerato int unsigned not null, 
idDocenteEncargado int unsigned ,
anioGre varchar (5),
idPensum int unsigned not null,
Seccion char -- A, B , C 
)engine=innodb; 

CREATE TABLE planificacionDetalle(
idPlanificacionDetalle int unsigned primary key auto_increment, 
idPlanificacion int unsigned not null,
idDocente int unsigned not null,
idMateria int unsigned not null,
anioGre varchar (5)
)engine=innodb; 

CREATE TABLE Horarios(
idHoraio int unsigned primary key auto_increment,
idPlanificacionDetalle int unsigned not null,
dia int    default 1, 
hora time  default '00:00:00'
)engine=innodb; -- HABRA QUE VALIDAR QUE NO EXISTA EL MISMO DOCENTE A LA MISMA HORA EN EL MISMO DIA  a la hora de insertar un nuevo detalle 

CREATE TABLE Matriculas(
idMatricula int unsigned primary key auto_increment,
idAlumno int unsigned not null,
idEncargado int unsigned not null,
afinidad_encargado varchar(80) not null,
idPlanificacion int unsigned not null, -- A LA HORA DE INGRESARLO LA SECRE DEBE VER EL NOMBRE DEL GRUPO
fechaMatricula datetime not null -- now()
)engine=innodb; 

CREATE TABLE Materias(
idMateria int unsigned primary key auto_increment,
nombreMateria varchar(100) not null, 
idCategoria int unsigned not null,
idAgrupacion int unsigned default null
)ENGINE=INNODB; 

CREATE TABLE agrupacionMaterias( -- Ciencias Naturales por ejemplo: incluyen fisica, quimica,etc. 
idAgrupacion int unsigned auto_increment primary key,
nombreAgrupacion varchar(100) not null
);
 
CREATE TABLE CategoriaMateria( -- Basicas, tecnicas etc. 
idCategoria int unsigned primary key auto_increment, 
nombreCategoria varchar(45) not null
)ENGINE=INNODB; 


CREATE TABLE Aprobaciones(
idAprobacion int unsigned primary key, -- entidad debil de alumno, es decir su id sera la misma del alumno (NIE) 
PrimerAño bool not null default false, 
SegundoAño bool not null default false, 
AplicaTBachillerato bool default false,
TercerAño bool not null default false,
promedioFinalPA float unsigned not null default 0, 
promedioFinalSA float unsigned not null default 0, 
promedioFinalTA float unsigned not null default 0
)ENGINE=INNODB; 
-- //////////////////////// Nuevas TABLAS 
Create table materiaperiodoconcretado(
idMaPeConcretado int unsigned primary key auto_increment,
nota float unsigned not null default 0,
periodo int unsigned default 1,
idMatricula int unsigned not null,
idActividad int unsigned not null
)engine=innodb;

CREATE TABLE rolUsuarios(
idRol int unsigned auto_increment primary key,
nombreRol varchar(50)
);
-- //////////////////////
CREATE TABLE Notas( 
idNota int unsigned primary key auto_increment,
-- idAlumno int unsigned not null, SE SUSTITUIRA POR ID_MATRICULA
idMatricula int unsigned NOT NULL,
idActividad int unsigned not null,
idMateria int unsigned not null,
Numeroperiodo int unsigned not null default 1,
ValorNota float not null default 0,
fechaAsignacionNota datetime not null,
ultimaModificacion datetime  
)ENGINE=INNODB; 

CREATE TABLE Actividades( -- Tendra quemadas las actividades segun la forma de trabajar con su respectivos porcentajes 
idActividad int unsigned primary key,
actividad varchar(100),
descripcion text, 
porcentaje decimal(10,4) not null
);


CREATE TABLE res_actividades(
idAc int unsigned primary key,
actividad varchar(100),
descripcion text, 
porcentaje decimal(10,2) not null
);
-- - ----------------------------------------------

CREATE TABLE Usuarios(
idUsuarios int unsigned primary key auto_increment, 
usuario varchar(40) unique, -- NO DEBE HABER USUARIOS REPETIDOS 
contrasenia varchar(150) not null,
idDocente int unsigned,
idRol int unsigned default 1 
)ENGINE=INNODB; 

CREATE TABLE Res_ModificacionNotas(
idNota int unsigned not null,
NotaVieja float unsigned not null default 0,
NotaNueva float unsigned not null default 0,
idActividad int unsigned not null,
idMateria int unsigned not null, 
idDocente int unsigned not null, 
fechaModificacion datetime not null,
tipoInsert char(1)
)ENGINE=INNODB; 

CREATE TABLE Res_Alumnos(
idAlumno int unsigned not null, 
nombre varchar(50) not null,
apellido varchar(50) not null,
estatus bool
)ENGINE=INNODB; 


create table fotoAlumnos(
idFoto int unsigned primary key auto_increment, 
fotoAlumno mediumblob,
idAlumno int unsigned not null,
anio varchar(4)
)engine=innodb; 

create table fotoTitulos(
idFotoTitulo int unsigned primary key auto_increment, 
fototitulo mediumblob, 
idAlumno int unsigned not null,
anio varchar(4) 
)engine=innodb; 


ALTER TABLE USUARIOS add foreign key (idRol) references rolusuarios (idRol);
ALTER TABLE usuarios add foreign key (idDocente) references docentes(idDocente);

ALTER TABLE Docentes ADD foreign key (idTitulo) references Titulos(idTitulo); 

ALTER TABLE Bachilleratos ADD foreign key (idEspecialidadBach) references EspecialidadBachillerato(idEspecialidadBach);

-- ALTER TABLE Bachilleratos ADD foreign key (idSeccion) references SeccionesBachillerato(idSeccion); 
ALTER TABLE Materias ADD foreign key (idCategoria) references CategoriaMateria(idCategoria); 
ALTER TABLE Materias ADD FOREIGN KEY (idAgrupacion) references agrupacionMaterias(idAgrupacion);

-- alter table notas add foreign key (idAlumno) references alumnos(NIE); ELIMINADA
ALTER TABLE Notas ADD foreign key (idActividad) references Actividades(idActividad); 
ALTER TABLE Notas ADD foreign key (idMateria) references Materias(idMateria);
ALTER TABLE Notas ADD foreign key (idMatricula) references Matriculas(idMatricula);


alter table aprobaciones add foreign key (idAprobacion) references alumnos(NIE);

ALTER TABLE USUARIOS add foreign key (idRol) references rolusuarios (idRol);

alter table materiaperiodoconcretado add foreign key(idMatricula) references matriculas(idMatricula);
alter table materiaperiodoconcretado add foreign key (idActividad) references actividades(idActividad);
ALTER TABLE fotoalumnos ADD foreign key (idAlumno) references Alumnos(NIE); 
ALTER TABLE fototitulos ADD foreign key (idAlumno) references Alumnos(NIE); 

alter table alumnos add foreign key (idDepartamento) references departamentos(iddepartamento);
alter table alumnos add foreign key (idmunicipio) references municipios(idmunicipio);
alter table municipios add foreign key (idDepartamento) references departamentos(iddepartamento);


create table Pensums(
idPensum int unsigned primary key auto_increment,
nombrePensum varchar(200) not null,
anioGre varchar (5),
DescripcionPensum text,
idBachillerato int unsigned not null
)Engine=innodb; 

create table pensumDetalle(
idPensumDetalle int unsigned not null, -- ID Pensum 
idMateria int unsigned not null
)engine=innodb;


create table ProNotas(
idProNotas int unsigned primary key auto_increment,
nombre varchar(100) not null, -- Ingreso de notas 2do Periodo por EJE
TiempoInicio datetime not null,
TiempoFinalizacion datetime not null,
fechaDesignacion datetime not null -- Este campo contendra la fecha en la que se hizo la programacion  
)engine=INNODB; 

create table resProNotas(
idProNotas int unsigned primary key auto_increment,
nombre varchar(100) not null,
TiempoInicio datetime not null,
TiempoFinalizacion datetime not null,
fechaDesignacion datetime not null, -- Este campo contendra la fecha en la que se hizo la programacion  
fechaEliminacion datetime not null
)engine =innodb;

-- /////////////////////////////// PARA NOTA PAES /////////////////////////

Create table NotaPaes(
idNotaPaes int primary key unique auto_increment, 
idAlumno int(11) unsigned not null,
anio varchar(5) not null,
NotaGlobal float default 0,
nLenguaje float default 0,
nMatematica float default 0,
nSociales float default 0,
nCiencias float default 0,
fecha datetime default now()
)engine=innodb;

ALTER TABLE NotaPaes add foreign key (idAlumno) references alumnos(NIE);


alter table pensums add foreign key (idBachillerato) references bachilleratos (idBachillerato);

alter table pensumDetalle add foreign key (idPensumDetalle) references pensums (idPensum);
alter table pensumDetalle add foreign key (idMateria) references Materias (idMateria);

alter table planificacion add foreign key (idPensum) references Pensums (idpensum); 
alter table planificacion add foreign key (idBachillerato) references bachilleratos (idbachillerato);
alter table planificacion add foreign key (idDocenteEncargado) references Docentes(idDocente);

alter table planificaciondetalle add foreign key (idDocente) references docentes(idDocente);
alter table planificaciondetalle add foreign key (idMateria) references Materias(idMateria);

--  PARA HORARIOS
alter table planificaciondetalle add foreign key (idPlanificacion) references planificacion(idPlanificacion); 
alter table Horarios add foreign key (idPlanificacionDetalle) references planificaciondetalle(idPlanificacionDetalle); 

alter table matriculas add foreign key (idAlumno) references alumnos (NIE);
alter table matriculas add foreign key (idEncargado) references encargados (idEncargado);
alter table matriculas add foreign key (idPlanificacion) references planificacion(idPlanificacion);