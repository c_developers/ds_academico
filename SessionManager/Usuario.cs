﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SessionManager
{
    public class Usuario
    {
        String _IDUsuario;
        String _Usuario;
        String _IDRol;
        String _Rol;
        String _IDDocente;
        String _Nombre;


        public String Docente
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }


        public String IDDocente
        {
            get { return _IDDocente; }
            set { _IDDocente = value; }
        }

        public String Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }

        public String IDRol
        {
            get { return _IDRol; }
            set { _IDRol = value; }
        }


        public String NombreUsuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public String IDUsuario
        {
            get { return _IDUsuario; }
            set { _IDUsuario = value; }
        }
    }
}
