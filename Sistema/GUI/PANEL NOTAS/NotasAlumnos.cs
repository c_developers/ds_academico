﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI.PANEL_NOTAS
{
    public partial class NotasAlumnos : Form
    {
        public String idMatricula, idMateria, periodo, NotaCien;
        private String[] ListaN;
        FrmNotas frm = new FrmNotas();
        private static bool ExisteNotaRe;

        public NotasAlumnos(String[] DatosNotas)
        {
            InitializeComponent();
            this.ListaN = DatosNotas;
            ExisteNotaRe = false;
        }
        public NotasAlumnos()
        {
            InitializeComponent();
            ExisteNotaRe = false;
        }

        private void Restringir()
        {
            if(getListaN()[4] == "4"){
                Activar();
            }else{
                Desactivar();
            }
           // ExisteNotaRe = ExisteNotaRepo();
           // CargarNotaRepo();
        }

        private void Desactivar()
        {
            this.lblRepo.Visible = false;
            this.lblExamyRepo.Visible = false;
            this.lblRepo.Enabled = false;
            this.lblExamyRepo.Enabled = false;
            this.txtRepo.Visible = false;
            this.txtRepo.Enabled = false;
            this.txtFinalRepo.Visible = false;
            this.txtFinalRepo.Enabled = false;

        }

        public void TraerNotaFinal()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select avg(nota) from materiaperiodoconcreto where idMateria='" + getListaN()[3] + @"' and idMatricula='" + getListaN()[2] + @"'");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
                Double valor = Convert.ToDouble(Resultado);
                if( valor >= 6)
                {
                    
                }
            }
            catch
            {
                Resultado = new DataTable();
            }

        }
        private void Activar()
        {
            this.lblRepo.Visible = true;
            this.lblExamyRepo.Visible = true;
            this.lblRepo.Enabled = true;
            this.lblExamyRepo.Enabled = true;
            this.txtRepo.Visible = true;
            this.txtRepo.Enabled = true;
            this.txtFinalRepo.Visible = true;
            this.txtFinalRepo.Enabled = true;

        }

        private void Listita()
        {
            this.idMatricula = getListaN()[2];
            this.idMateria = getListaN()[3];
            this.periodo = getListaN()[4];
            lblName.Text = getListaN()[0];
            lblGroup.Text = getListaN()[1];
            lblPrincipal.Text = getListaN()[12];
            txtA1.Text = getListaN()[5];
            txtA2.Text = getListaN()[6];
            txtA3.Text = getListaN()[7];
            txtA4.Text = getListaN()[8];
            txtA5.Text = getListaN()[9];
            txtA6.Text = getListaN()[10];
            txtA7.Text = getListaN()[11];

        }

        public String[] getListaN()
        {
            return ListaN;
        }

        public void setListaN(String[] ListaN)
        {
            this.ListaN = ListaN;
            Listita();
        }


        private void CalculoInstantaneo()
        {
            if (!ValidacionErrores())
            {
                
            }
        }

        private String NuevoPromedio()
        {
            String re = "";
            Double n1, n2, n3, n4, n5, n6, n7, n100;
            n1 = Double.Parse(txtA1.Text);
            n2 = Double.Parse(txtA2.Text);
            n3 = Double.Parse(txtA3.Text);
            n4 = Double.Parse(txtA4.Text);
            n5 = Double.Parse(txtA5.Text);
            n6 = Double.Parse(txtA6.Text);
            n7 = Double.Parse(txtA7.Text);
            n100 = (n1 * 0.1250) + (n2 * 0.1250) + (n3 * 0.1250) + (n4 * 0.1250) + (n5 * 0.10) + (n6 * 0.1) + (n7 * 0.30);
            return re;
        }
        private String[] TraerIDNotas(String idMatricula, String periodo, String idMateria)
        {
            String[] resultado = new string[7];
            DataTable Notas = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    Notas = oOperacion.Consultar(@"select idNota from notas where idMatricula='" + idMatricula + @"'and idActividad='" + (i + 1) + @"'and idMateria='" + idMateria + @"'and Numeroperiodo='" + periodo + @"'");
                    resultado[i] = Notas.Rows[0][0].ToString();
                }
            }
            catch
            {
                MessageBox.Show("Error en algoritmo traer id notas");
            }
            return resultado;
        }



        private String TraerIDMPC(String idMatricula, String Periodo, String idMateria)
        {
            String Resultado = "";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            DataTable Notas = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append(@"select idMateConcreto from MateriaPeriodoConcreto where idMatricula='" + idMatricula + @"' and idMateria='" + idMateria + @"' and periodo ='" + periodo + @"'");
            try
            {
                Notas = oOperacion.Consultar(Sentencia.ToString());
                Resultado = Notas.Rows[0][0].ToString();

            }
            catch
            {

            }
            return Resultado;
        }


        private void ActualizandoMPC(String id, String nueva)
        {
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                int act = oOperacion.Actualizar(@"Update Materiaperiodoconcreto set nota='" + nueva + @"' where idMateConcreto='" + id + @"'");
            }
            catch
            {

            }
        }

        private void NotasAlumnos_Load(object sender, EventArgs e)
        {
            Listita();
        }

        private void Actualizando(String n1, String n2, String n3, String n4, String n5, String n6, String n7, String[] id)
        {
            int c = 0;
            String[] notas = new string[7];
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            //String Plantilla = @"update notas set ultimamodificacion=now(),valorNota=";
            notas[0] = n1;
            notas[1] = n2;
            notas[2] = n3;
            notas[3] = n4;
            notas[4] = n5;
            notas[5] = n6;
            notas[6] = n7;
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    c += oOperacion.Actualizar(@"update notas set ultimamodificacion=now(),valorNota='" + notas[i] + @"' where idNota='" + id[i] + @"'");
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Error" + ex);
            }
            MessageBox.Show("Numero de notas actualizadas" + c);
        }
        private Boolean ValidacionErrores()
        {
            bool f = false;

            float n1, n2, n3, n4, n5, n6, n7 = 0;
            if(this.txtA1.Text == "" || this.txtA2.Text == "" || this.txtA3.Text =="" || this.txtA4.Text == "" || this.txtA5.Text == "" || this.txtA6.Text == "" || this.txtA7.Text == "")
            {
                f = true;
            }
            else
            {
                try
                {
                    n1 = float.Parse(this.txtA1.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n2 = float.Parse(this.txtA2.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n3 = float.Parse(this.txtA3.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n4 = float.Parse(this.txtA4.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n5 = float.Parse(this.txtA5.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n6 = float.Parse(this.txtA6.Text, CultureInfo.InvariantCulture.NumberFormat);
                    n7 = float.Parse(this.txtA7.Text, CultureInfo.InvariantCulture.NumberFormat);
                    if (n1 > 10 || n1 < 0 || n2 > 10 || n2 < 0 || n3 > 10 || n3 < 0 || n4 > 10 || n4 < 0 || n5 > 10 || n5 < 0 || n6 > 10 || n6 < 0 || n7 > 10 || n7 < 0)
                    {
                        f = true;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error" + e);
                }
            }
            return f;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
           if (ValidacionErrores())
            {
                MessageBox.Show("Verifique las notas");
            }
            else
            {
                String n1, n2, n3, n4, n5, n6, n7;
                n1 = this.txtA1.Text;
                n2 = this.txtA2.Text;
                n3 = this.txtA3.Text;
                n4 = this.txtA4.Text;
                n5 = this.txtA5.Text;
                n6 = this.txtA6.Text;
                n7 = this.txtA7.Text;
               // int p = Convert.ToInt32(periodo);
                Actualizando(n1, n2, n3, n4, n5, n6, n7, this.TraerIDNotas(idMatricula, periodo, idMateria));
                ActualizandoMPC(this.TraerIDMPC(idMatricula, periodo, idMateria), this.NotaCien);
                this.Dispose();
                FrmNotas.ActualizarTabla(idMateria, Convert.ToInt32(periodo), frm.grupo);
                frm.ActualizarGrupos(frm.txtNIP.Text, frm.txtAnio.Text);

            }

        }

    }
}
