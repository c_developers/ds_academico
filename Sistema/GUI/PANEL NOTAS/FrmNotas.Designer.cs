﻿namespace Sistema.GUI.PANEL_NOTAS
{
    partial class FrmNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblNombre = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtAnio = new System.Windows.Forms.TextBox();
            this.lblAnio = new System.Windows.Forms.Label();
            this.lblNip = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblRol = new System.Windows.Forms.Label();
            this.lblApe = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbt1 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNIP = new System.Windows.Forms.TextBox();
            this.lblPeriodos = new System.Windows.Forms.Label();
            this.cmbMaterias = new System.Windows.Forms.ComboBox();
            this.lblMaterias = new System.Windows.Forms.Label();
            this.comboBach = new System.Windows.Forms.ComboBox();
            this.lblGrupos = new System.Windows.Forms.Label();
            this.lblNusis = new System.Windows.Forms.Label();
            this.dtgvDatos = new System.Windows.Forms.DataGridView();
            this.idAlumno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Act7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actividades = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Promedio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idMatricula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errp = new System.Windows.Forms.ErrorProvider(this.components);
            this.rbt2 = new System.Windows.Forms.RadioButton();
            this.rbt3 = new System.Windows.Forms.RadioButton();
            this.rbt4 = new System.Windows.Forms.RadioButton();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblHorario = new System.Windows.Forms.Label();
            this.lblNotas = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errp)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(3, 162);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(266, 23);
            this.lblNombre.TabIndex = 6;
            this.lblNombre.Text = "Nombre:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(0, 749);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.txtAnio);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.lblAnio);
            this.panel1.Controls.Add(this.lblNip);
            this.panel1.Controls.Add(this.lblID);
            this.panel1.Controls.Add(this.lblRole);
            this.panel1.Controls.Add(this.lblRol);
            this.panel1.Controls.Add(this.lblApe);
            this.panel1.Controls.Add(this.lblNom);
            this.panel1.Controls.Add(this.btnSalir);
            this.panel1.Controls.Add(this.lblUser);
            this.panel1.Controls.Add(this.lblNombre);
            this.panel1.Controls.Add(this.lblHorario);
            this.panel1.Controls.Add(this.lblNotas);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 749);
            this.panel1.TabIndex = 7;
            // 
            // txtAnio
            // 
            this.txtAnio.Location = new System.Drawing.Point(57, 383);
            this.txtAnio.Multiline = true;
            this.txtAnio.Name = "txtAnio";
            this.txtAnio.Size = new System.Drawing.Size(144, 30);
            this.txtAnio.TabIndex = 12;
            this.txtAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblAnio
            // 
            this.lblAnio.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnio.Location = new System.Drawing.Point(5, 357);
            this.lblAnio.Name = "lblAnio";
            this.lblAnio.Size = new System.Drawing.Size(264, 23);
            this.lblAnio.TabIndex = 13;
            this.lblAnio.Text = "AÑO:";
            this.lblAnio.Click += new System.EventHandler(this.lblAnio_Click);
            // 
            // lblNip
            // 
            this.lblNip.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNip.Location = new System.Drawing.Point(53, 323);
            this.lblNip.Name = "lblNip";
            this.lblNip.Size = new System.Drawing.Size(216, 23);
            this.lblNip.TabIndex = 12;
            this.lblNip.Text = ".";
            // 
            // lblID
            // 
            this.lblID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(5, 300);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(264, 23);
            this.lblID.TabIndex = 11;
            this.lblID.Text = "ID ó NIP:";
            // 
            // lblRole
            // 
            this.lblRole.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.Location = new System.Drawing.Point(53, 266);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(216, 23);
            this.lblRole.TabIndex = 10;
            this.lblRole.Text = ".";
            // 
            // lblRol
            // 
            this.lblRol.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRol.Location = new System.Drawing.Point(5, 243);
            this.lblRol.Name = "lblRol";
            this.lblRol.Size = new System.Drawing.Size(264, 23);
            this.lblRol.TabIndex = 9;
            this.lblRol.Text = "Rol:";
            // 
            // lblApe
            // 
            this.lblApe.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApe.Location = new System.Drawing.Point(53, 208);
            this.lblApe.Name = "lblApe";
            this.lblApe.Size = new System.Drawing.Size(216, 23);
            this.lblApe.TabIndex = 8;
            this.lblApe.Text = ".";
            // 
            // lblNom
            // 
            this.lblNom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNom.Location = new System.Drawing.Point(53, 185);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(216, 23);
            this.lblNom.TabIndex = 7;
            this.lblNom.Text = ".";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel2.Controls.Add(this.btnExportarExcel);
            this.panel2.Controls.Add(this.rbt4);
            this.panel2.Controls.Add(this.rbt3);
            this.panel2.Controls.Add(this.rbt2);
            this.panel2.Controls.Add(this.rbt1);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.txtNIP);
            this.panel2.Controls.Add(this.lblPeriodos);
            this.panel2.Controls.Add(this.cmbMaterias);
            this.panel2.Controls.Add(this.lblMaterias);
            this.panel2.Controls.Add(this.comboBach);
            this.panel2.Controls.Add(this.lblGrupos);
            this.panel2.Controls.Add(this.lblNusis);
            this.panel2.Controls.Add(this.dtgvDatos);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(272, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1098, 749);
            this.panel2.TabIndex = 8;
            // 
            // rbt1
            // 
            this.rbt1.AutoSize = true;
            this.rbt1.Location = new System.Drawing.Point(605, 137);
            this.rbt1.Name = "rbt1";
            this.rbt1.Size = new System.Drawing.Size(70, 17);
            this.rbt1.TabIndex = 16;
            this.rbt1.TabStop = true;
            this.rbt1.Text = "Periodo 1";
            this.rbt1.UseVisualStyleBackColor = true;
            this.rbt1.CheckedChanged += new System.EventHandler(this.rbt1_CheckedChanged_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(420, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtNIP
            // 
            this.txtNIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNIP.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtNIP.Location = new System.Drawing.Point(128, 27);
            this.txtNIP.Multiline = true;
            this.txtNIP.Name = "txtNIP";
            this.txtNIP.Size = new System.Drawing.Size(209, 30);
            this.txtNIP.TabIndex = 14;
            this.txtNIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNIP_KeyPress);
            this.txtNIP.Validated += new System.EventHandler(this.txtNIP_Validated);
            // 
            // lblPeriodos
            // 
            this.lblPeriodos.AutoSize = true;
            this.lblPeriodos.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodos.Location = new System.Drawing.Point(465, 134);
            this.lblPeriodos.Name = "lblPeriodos";
            this.lblPeriodos.Size = new System.Drawing.Size(101, 19);
            this.lblPeriodos.TabIndex = 7;
            this.lblPeriodos.Text = "PERIODOS:";
            // 
            // cmbMaterias
            // 
            this.cmbMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbMaterias.FormattingEnabled = true;
            this.cmbMaterias.ItemHeight = 13;
            this.cmbMaterias.Location = new System.Drawing.Point(128, 132);
            this.cmbMaterias.Name = "cmbMaterias";
            this.cmbMaterias.Size = new System.Drawing.Size(260, 21);
            this.cmbMaterias.TabIndex = 6;
            this.cmbMaterias.SelectedIndexChanged += new System.EventHandler(this.cmbMaterias_SelectedIndexChanged);
            // 
            // lblMaterias
            // 
            this.lblMaterias.AutoSize = true;
            this.lblMaterias.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaterias.Location = new System.Drawing.Point(19, 132);
            this.lblMaterias.Name = "lblMaterias";
            this.lblMaterias.Size = new System.Drawing.Size(103, 19);
            this.lblMaterias.TabIndex = 5;
            this.lblMaterias.Text = "MATERIAS:";
            // 
            // comboBach
            // 
            this.comboBach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBach.FormattingEnabled = true;
            this.comboBach.ItemHeight = 13;
            this.comboBach.Location = new System.Drawing.Point(128, 88);
            this.comboBach.Name = "comboBach";
            this.comboBach.Size = new System.Drawing.Size(609, 21);
            this.comboBach.TabIndex = 4;
            this.comboBach.SelectedIndexChanged += new System.EventHandler(this.comboBach_SelectedIndexChanged);
            // 
            // lblGrupos
            // 
            this.lblGrupos.AutoSize = true;
            this.lblGrupos.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupos.Location = new System.Drawing.Point(19, 88);
            this.lblGrupos.Name = "lblGrupos";
            this.lblGrupos.Size = new System.Drawing.Size(84, 19);
            this.lblGrupos.TabIndex = 3;
            this.lblGrupos.Text = "GRUPOS:";
            // 
            // lblNusis
            // 
            this.lblNusis.AutoSize = true;
            this.lblNusis.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNusis.Location = new System.Drawing.Point(19, 30);
            this.lblNusis.Name = "lblNusis";
            this.lblNusis.Size = new System.Drawing.Size(66, 19);
            this.lblNusis.TabIndex = 1;
            this.lblNusis.Text = "NUSIS:";
            // 
            // dtgvDatos
            // 
            this.dtgvDatos.AllowUserToAddRows = false;
            this.dtgvDatos.AllowUserToDeleteRows = false;
            this.dtgvDatos.AllowUserToResizeRows = false;
            this.dtgvDatos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvDatos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvDatos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDatos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idAlumno,
            this.Nombre,
            this.Act1,
            this.Act2,
            this.Act3,
            this.Act4,
            this.Act5,
            this.Act6,
            this.Act7,
            this.PO,
            this.Actividades,
            this.HE,
            this.Promedio,
            this.idMatricula});
            this.dtgvDatos.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtgvDatos.Location = new System.Drawing.Point(0, 175);
            this.dtgvDatos.MultiSelect = false;
            this.dtgvDatos.Name = "dtgvDatos";
            this.dtgvDatos.RowHeadersVisible = false;
            this.dtgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvDatos.Size = new System.Drawing.Size(1095, 571);
            this.dtgvDatos.TabIndex = 0;
            this.dtgvDatos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvDatos_CellDoubleClick);
            this.dtgvDatos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dtgvDatos_CellFormatting);
            // 
            // idAlumno
            // 
            this.idAlumno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.idAlumno.DataPropertyName = "idAlumno";
            this.idAlumno.HeaderText = "NIE";
            this.idAlumno.Name = "idAlumno";
            this.idAlumno.ReadOnly = true;
            // 
            // Nombre
            // 
            this.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 69;
            // 
            // Act1
            // 
            this.Act1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Act1.DataPropertyName = "Act1";
            this.Act1.HeaderText = "Act1";
            this.Act1.Name = "Act1";
            this.Act1.ReadOnly = true;
            // 
            // Act2
            // 
            this.Act2.DataPropertyName = "Act2";
            this.Act2.HeaderText = "Act2";
            this.Act2.Name = "Act2";
            this.Act2.ReadOnly = true;
            // 
            // Act3
            // 
            this.Act3.DataPropertyName = "Act3";
            this.Act3.HeaderText = "Act3";
            this.Act3.Name = "Act3";
            this.Act3.ReadOnly = true;
            // 
            // Act4
            // 
            this.Act4.DataPropertyName = "Act4";
            this.Act4.HeaderText = "Act4";
            this.Act4.Name = "Act4";
            this.Act4.ReadOnly = true;
            // 
            // Act5
            // 
            this.Act5.DataPropertyName = "Act5";
            this.Act5.HeaderText = "Act5";
            this.Act5.Name = "Act5";
            this.Act5.ReadOnly = true;
            // 
            // Act6
            // 
            this.Act6.DataPropertyName = "Act6";
            this.Act6.HeaderText = "Act6";
            this.Act6.Name = "Act6";
            this.Act6.ReadOnly = true;
            // 
            // Act7
            // 
            this.Act7.DataPropertyName = "Act7";
            this.Act7.HeaderText = "EXAM_F";
            this.Act7.Name = "Act7";
            this.Act7.ReadOnly = true;
            // 
            // PO
            // 
            this.PO.DataPropertyName = "PO";
            this.PO.HeaderText = "30%";
            this.PO.Name = "PO";
            this.PO.ReadOnly = true;
            // 
            // Actividades
            // 
            this.Actividades.DataPropertyName = "Actividades";
            this.Actividades.HeaderText = "50%";
            this.Actividades.Name = "Actividades";
            this.Actividades.ReadOnly = true;
            // 
            // HE
            // 
            this.HE.DataPropertyName = "HE";
            this.HE.HeaderText = "20%";
            this.HE.Name = "HE";
            this.HE.ReadOnly = true;
            // 
            // Promedio
            // 
            this.Promedio.DataPropertyName = "Promedio";
            this.Promedio.HeaderText = "100%";
            this.Promedio.Name = "Promedio";
            this.Promedio.ReadOnly = true;
            // 
            // idMatricula
            // 
            this.idMatricula.DataPropertyName = "idMatricula";
            this.idMatricula.HeaderText = "idMatricula";
            this.idMatricula.Name = "idMatricula";
            this.idMatricula.ReadOnly = true;
            this.idMatricula.Visible = false;
            // 
            // errp
            // 
            this.errp.ContainerControl = this;
            // 
            // rbt2
            // 
            this.rbt2.AutoSize = true;
            this.rbt2.Location = new System.Drawing.Point(681, 136);
            this.rbt2.Name = "rbt2";
            this.rbt2.Size = new System.Drawing.Size(70, 17);
            this.rbt2.TabIndex = 17;
            this.rbt2.TabStop = true;
            this.rbt2.Text = "Periodo 2";
            this.rbt2.UseVisualStyleBackColor = true;
            // 
            // rbt3
            // 
            this.rbt3.AutoSize = true;
            this.rbt3.Location = new System.Drawing.Point(757, 137);
            this.rbt3.Name = "rbt3";
            this.rbt3.Size = new System.Drawing.Size(70, 17);
            this.rbt3.TabIndex = 18;
            this.rbt3.TabStop = true;
            this.rbt3.Text = "Periodo 3";
            this.rbt3.UseVisualStyleBackColor = true;
            // 
            // rbt4
            // 
            this.rbt4.AutoSize = true;
            this.rbt4.Location = new System.Drawing.Point(833, 137);
            this.rbt4.Name = "rbt4";
            this.rbt4.Size = new System.Drawing.Size(70, 17);
            this.rbt4.TabIndex = 19;
            this.rbt4.TabStop = true;
            this.rbt4.Text = "Periodo 4";
            this.rbt4.UseVisualStyleBackColor = true;
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Image = global::Sistema.Properties.Resources.Excel_2013_23480;
            this.btnExportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportarExcel.Location = new System.Drawing.Point(803, 73);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(126, 38);
            this.btnExportarExcel.TabIndex = 20;
            this.btnExportarExcel.Text = "Exportar a Excel";
            this.btnExportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Image = global::Sistema.Properties.Resources.close;
            this.btnCerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrar.Location = new System.Drawing.Point(84, 665);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(182, 55);
            this.btnCerrar.TabIndex = 15;
            this.btnCerrar.Text = "Cerrar Sesion";
            this.btnCerrar.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.SystemColors.HighlightText;
            this.btnSalir.Image = global::Sistema.Properties.Resources.left_arrow_1;
            this.btnSalir.Location = new System.Drawing.Point(0, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(113, 43);
            this.btnSalir.TabIndex = 1;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblUser
            // 
            this.lblUser.Image = global::Sistema.Properties.Resources.user;
            this.lblUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUser.Location = new System.Drawing.Point(3, 61);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(215, 46);
            this.lblUser.TabIndex = 2;
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHorario
            // 
            this.lblHorario.Image = global::Sistema.Properties.Resources.schedule;
            this.lblHorario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHorario.Location = new System.Drawing.Point(231, 61);
            this.lblHorario.Name = "lblHorario";
            this.lblHorario.Size = new System.Drawing.Size(38, 46);
            this.lblHorario.TabIndex = 3;
            this.lblHorario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNotas
            // 
            this.lblNotas.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotas.Image = global::Sistema.Properties.Resources.login;
            this.lblNotas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNotas.Location = new System.Drawing.Point(24, 116);
            this.lblNotas.Name = "lblNotas";
            this.lblNotas.Size = new System.Drawing.Size(245, 35);
            this.lblNotas.TabIndex = 5;
            this.lblNotas.Text = "INSERCION DE NOTAS";
            this.lblNotas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "FrmNotas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Notas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmNotas_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblNotas;
        private System.Windows.Forms.Label lblHorario;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblApe;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblAnio;
        private System.Windows.Forms.Label lblNip;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblRol;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblGrupos;
        private System.Windows.Forms.Label lblNusis;
        private System.Windows.Forms.Label lblPeriodos;
        private System.Windows.Forms.ComboBox cmbMaterias;
        private System.Windows.Forms.Label lblMaterias;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ErrorProvider errp;
        public System.Windows.Forms.ComboBox comboBach;
        public System.Windows.Forms.DataGridView dtgvDatos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAlumno;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Act7;
        private System.Windows.Forms.DataGridViewTextBoxColumn PO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actividades;
        private System.Windows.Forms.DataGridViewTextBoxColumn HE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Promedio;
        private System.Windows.Forms.DataGridViewTextBoxColumn idMatricula;
        private System.Windows.Forms.RadioButton rbt1;
        private System.Windows.Forms.RadioButton rbt4;
        private System.Windows.Forms.RadioButton rbt3;
        private System.Windows.Forms.RadioButton rbt2;
        private System.Windows.Forms.Button btnExportarExcel;
        public System.Windows.Forms.TextBox txtAnio;
        public System.Windows.Forms.TextBox txtNIP;
    }
}