﻿namespace Sistema.GUI.PANEL_NOTAS
{
    partial class NotasAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrincipal = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtA1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtA2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtA3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtA4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtA5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtA6 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtA7 = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPeriodo = new System.Windows.Forms.Label();
            this.lblRepo = new System.Windows.Forms.Label();
            this.txtRepo = new System.Windows.Forms.TextBox();
            this.lblExamyRepo = new System.Windows.Forms.Label();
            this.txtFinalRepo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Image = global::Sistema.Properties.Resources.study__1_;
            this.lblPrincipal.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblPrincipal.Location = new System.Drawing.Point(0, 0);
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Size = new System.Drawing.Size(712, 82);
            this.lblPrincipal.TabIndex = 1;
            this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(124, 132);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(177, 143);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(278, 28);
            this.lblName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(124, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Grupo:";
            // 
            // lblGroup
            // 
            this.lblGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroup.Location = new System.Drawing.Point(177, 195);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(278, 28);
            this.lblGroup.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 255);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Actividad 1:";
            // 
            // txtA1
            // 
            this.txtA1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA1.Location = new System.Drawing.Point(159, 252);
            this.txtA1.Multiline = true;
            this.txtA1.Name = "txtA1";
            this.txtA1.Size = new System.Drawing.Size(68, 26);
            this.txtA1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Actividad 2:";
            // 
            // txtA2
            // 
            this.txtA2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA2.Location = new System.Drawing.Point(421, 252);
            this.txtA2.Multiline = true;
            this.txtA2.Name = "txtA2";
            this.txtA2.Size = new System.Drawing.Size(68, 26);
            this.txtA2.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 311);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Actividad 3:";
            // 
            // txtA3
            // 
            this.txtA3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA3.Location = new System.Drawing.Point(159, 308);
            this.txtA3.Multiline = true;
            this.txtA3.Name = "txtA3";
            this.txtA3.Size = new System.Drawing.Size(68, 26);
            this.txtA3.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(320, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Actividad 4:";
            // 
            // txtA4
            // 
            this.txtA4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA4.Location = new System.Drawing.Point(421, 308);
            this.txtA4.Multiline = true;
            this.txtA4.Name = "txtA4";
            this.txtA4.Size = new System.Drawing.Size(68, 26);
            this.txtA4.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 362);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Auto Evaluacion:";
            // 
            // txtA5
            // 
            this.txtA5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA5.Location = new System.Drawing.Point(159, 359);
            this.txtA5.Multiline = true;
            this.txtA5.Name = "txtA5";
            this.txtA5.Size = new System.Drawing.Size(68, 26);
            this.txtA5.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(320, 362);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Hetero Evaluacion:";
            // 
            // txtA6
            // 
            this.txtA6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA6.Location = new System.Drawing.Point(421, 359);
            this.txtA6.Multiline = true;
            this.txtA6.Name = "txtA6";
            this.txtA6.Size = new System.Drawing.Size(68, 26);
            this.txtA6.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 417);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Examen Final:";
            // 
            // txtA7
            // 
            this.txtA7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtA7.Location = new System.Drawing.Point(159, 414);
            this.txtA7.Multiline = true;
            this.txtA7.Name = "txtA7";
            this.txtA7.Size = new System.Drawing.Size(68, 26);
            this.txtA7.TabIndex = 19;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(553, 447);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(111, 46);
            this.btnGuardar.TabIndex = 20;
            this.btnGuardar.Text = "button1";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(124, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Periodo:";
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(177, 92);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.Size = new System.Drawing.Size(206, 28);
            this.lblPeriodo.TabIndex = 22;
            // 
            // lblRepo
            // 
            this.lblRepo.AutoSize = true;
            this.lblRepo.Location = new System.Drawing.Point(58, 464);
            this.lblRepo.Name = "lblRepo";
            this.lblRepo.Size = new System.Drawing.Size(63, 13);
            this.lblRepo.TabIndex = 23;
            this.lblRepo.Text = "Reposicion:";
            // 
            // txtRepo
            // 
            this.txtRepo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRepo.Location = new System.Drawing.Point(159, 461);
            this.txtRepo.Multiline = true;
            this.txtRepo.Name = "txtRepo";
            this.txtRepo.Size = new System.Drawing.Size(68, 26);
            this.txtRepo.TabIndex = 24;
            // 
            // lblExamyRepo
            // 
            this.lblExamyRepo.AutoSize = true;
            this.lblExamyRepo.Location = new System.Drawing.Point(307, 417);
            this.lblExamyRepo.Name = "lblExamyRepo";
            this.lblExamyRepo.Size = new System.Drawing.Size(111, 13);
            this.lblExamyRepo.TabIndex = 25;
            this.lblExamyRepo.Text = "Ex. Final y Reposicion";
            // 
            // txtFinalRepo
            // 
            this.txtFinalRepo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalRepo.Location = new System.Drawing.Point(421, 414);
            this.txtFinalRepo.Multiline = true;
            this.txtFinalRepo.Name = "txtFinalRepo";
            this.txtFinalRepo.Size = new System.Drawing.Size(68, 26);
            this.txtFinalRepo.TabIndex = 26;
            // 
            // NotasAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 508);
            this.Controls.Add(this.txtFinalRepo);
            this.Controls.Add(this.lblExamyRepo);
            this.Controls.Add(this.txtRepo);
            this.Controls.Add(this.lblRepo);
            this.Controls.Add(this.lblPeriodo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtA7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtA6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtA5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtA4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtA3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtA2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtA1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblPrincipal);
            this.Name = "NotasAlumnos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NotasAlumnos";
            this.Load += new System.EventHandler(this.NotasAlumnos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPrincipal;
        private System.Windows.Forms.Label lblNombre;
        public System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtA1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtA2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtA3;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtA4;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtA5;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtA6;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtA7;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblPeriodo;
        private System.Windows.Forms.Label lblRepo;
        public System.Windows.Forms.TextBox txtRepo;
        private System.Windows.Forms.Label lblExamyRepo;
        public System.Windows.Forms.TextBox txtFinalRepo;
    }
}