﻿using CacheManager;
using SessionManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI.PANEL_NOTAS
{
    public partial class FrmNotas : Form
    {
        private ArrayList ListaGrupos = new ArrayList();
        private ArrayList ListaMaterias = new ArrayList();
        Usuario user = new Usuario();
        private NotasAlumnos ventanaAlumnos;
        //NotasAlumnos ventanaAlumnos= new NotasAlumnos();
        public String[] ListaNotas = new string[14];
        public static DataTable Resultado = new DataTable();
       // FrmNotas frm = new FrmNotas();
        BindingSource Notas = new BindingSource();
        public  String grupo, Materia;
        private static Boolean Procedimiento;
        int Periodo ;

        public FrmNotas()
        {
            InitializeComponent();
            txtAnio.Text = DateTime.Now.ToString("yyyy");
            CacheManager.SystemCache.validar(this);
        }

        public FrmNotas(Usuario obj)
        {
            this.user = obj;
            this.ListaMaterias = new ArrayList();
            this.ListaGrupos = new ArrayList();
            ventanaAlumnos = new NotasAlumnos();
            FrmNotas.Resultado = new DataTable();
            if(Convert.ToInt32(user.IDRol) == 1)
            {
                MessageBox.Show("Bienvenido Administrador");
            }
            this.lblNom.Text = user.NombreUsuario;
        }

        //Funcion que me permite actualizar la lista de grupos a partir del NIP ingresado.
        public void ActualizarGrupos(String NIP, String anio)
        {
            this.comboBach.Items.Clear();
            this.ListaGrupos.Clear();
            StringBuilder con = new StringBuilder();
            con.Append(@"select a.idPlanificacion, a.nombreGrupo as 'Grupo', d.nombre,d.apellido from planificacion a,  planificaciondetalle b, docentes d where a.idPlanificacion = b.idPlanificacion and b.idDocente = d.idDocente and d.NIDocente = '" + NIP + @"' and b.anioGre = '" + anio + @"'group by a.idPlanificacion");
            DBManager.CLS.DBOperacion operacion = new DBManager.CLS.DBOperacion();
            try
            {
                DataTable dt = operacion.Consultar(con.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ListaGrupos.Add(dt.Rows[i]["idPlanificacion"].ToString());
                        comboBach.Items.Add(dt.Rows[i]["Grupo"].ToString());

                    }
                }
                else
                {
                    ListaGrupos = null;
                }

            }
            catch
            {
                this.comboBach.Items.Clear();
            }
        }
        //Procedimiento que me permite actualizar la lista de materias.
        public void ActualizarMaterias(String idGrupo, String doce)
        {
            this.cmbMaterias.Items.Clear();
            this.ListaMaterias.Clear();
            //ArrayList Resultado = new ArrayList();
            StringBuilder con = new StringBuilder();
            con.Append(@"select b.idMateria ,c.nombreMateria as 'materia'
                        from planificacion a, planificaciondetalle b, materias c, docentes d
                        where d.idDocente=b.idDocente and a.idPlanificacion=b.idPlanificacion
                        and c.idMateria=b.idMateria and d.NIDocente='" + doce + @"' and a.idPlanificacion='" + idGrupo + @"'");
            DBManager.CLS.DBOperacion operacion = new DBManager.CLS.DBOperacion();
            try
            {
                DataTable dt = operacion.Consultar(con.ToString());
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        this.ListaMaterias.Add(dt.Rows[i]["idMateria"].ToString());
                        this.cmbMaterias.Items.Add(dt.Rows[i]["materia"].ToString());
                    }
                }
                else
                {
                    this.ListaMaterias = null;
                }

            }
            catch
            {

                MessageBox.Show("Error al cargar el combobox");
                this.cmbMaterias.Items.Clear();
            }

        }
        //Funcion que me permite extraer el periodo seleccionado por el usuario.
        private int RadioSeleccionado()
        {
            if (this.rbt1.Checked)
            {
                Periodo = 1;
            }
            if (this.rbt2.Checked)
            {
                Periodo = 2;
            }
            if (this.rbt3.Checked)
            {
                Periodo = 3;
            }
            if (this.rbt4.Checked)
            {
                Periodo  = 4;
            }
   
            return Periodo;
        }
        private static void LimpiarTable(DataTable modelo)
        {
            int a = modelo.Rows.Count;

            for(int i=0; i < a; i++)
            {
                modelo.Rows.Clear();
            }


        }
        //Funcion que me permite rellenar la tabla con notas a partir de los parametros Grupo y Materia.
        public static DataTable ActualizarTabla(String IDMateria, int periodo, String IDGrupo)
        {
            //LimpiarTable(FrmNotas.Resultado);
            //DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder(); ///Constructor de cadena.
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT distinct z.idAlumno,concat(alm.PApellido,' ',alm.SApellido,',',' ',alm.PNombre,' ',alm.SNombre) AS 'Nombre',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=1 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act1',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=2 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act2',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=3 and a.idMateria=x.idMateria and  a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act3',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=4 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act4',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=5 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act5',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=6 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act6',
                                round((SELECT a.ValorNota FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=7 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Act7',
                                round((SELECT (SUM(a.ValorNota*b.porcentaje)) FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad between 5 AND 6 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'HE',
                                round((SELECT (SUM(a.ValorNota*b.porcentaje)) FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad between 1 AND 4 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Actividades',
                                round((SELECT (Sum(a.ValorNota*b.porcentaje)) FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad=7 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'PO',   
                                round((SELECT (SUM(a.ValorNota*b.porcentaje)) FROM notas a, actividades b WHERE a.idActividad=b.idActividad AND z.idMatricula=a.idMatricula
                                and a.idActividad between 1 AND 7 and a.idMateria=x.idMateria and a.Numeroperiodo=x.Numeroperiodo),2) AS 'Promedio',
                                z.idMatricula
                                FROM notas x, actividades y , matriculas z, planificacion pl, alumnos alm
                                WHERE x.idActividad=y.idActividad AND x.idMatricula=z.idMatricula  
                                AND pl.idPlanificacion=z.idPlanificacion AND alm.NIE=z.idAlumno 
                                and x.idMateria='" + IDMateria + @"'AND x.Numeroperiodo='" + periodo + @"'AND pl.idPlanificacion='" + IDGrupo + @"'and x.idActividad=1 order by alm.PApellido,alm.PApellido,alm.Sapellido asc;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }

       private String[] TraerIDNotas(String idMatricula, String periodo, String idMateria)
        {
            String[] resultado = new string[7];
            DataTable Notas = new DataTable();
            //StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    Notas = oOperacion.Consultar(@"select idNota from notas where idMatricula='" + idMatricula + @"'and idActividad='" + (i + 1) + @"'and idMateria='" + idMateria + @"'and Numeroperiodo='" + periodo + @"'");
                    resultado[i] = Notas.Rows[0][0].ToString();
                }
            }
            catch
            {
                MessageBox.Show("Error en algoritmo traer id notas");
            }
            return resultado;
        }



        private String TraerIDMPC(String idMatricula, String Periodo, String idMateria)
        {
            String Resultado = "";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            DataTable Notas = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append(@"select idMateConcreto from MateriaPeriodoConcreto where idMatricula='" + idMatricula + @"' and idMateria='" + idMateria + @"' and periodo ='" + Periodo + @"'");
            try
            {
                Notas = oOperacion.Consultar(Sentencia.ToString());
                Resultado = Notas.Rows[0][0].ToString();

            }
            catch
            {

            }
            return Resultado;
        }


        private void ActualizandoMPC(String id, String nueva)
        {
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                int act = oOperacion.Actualizar(@"Update Materiaperiodoconcreto set nota='" + nueva + @"' where idMateConcreto='" + id + @"'");
            }
            catch
            {

            }
        }

        private void Actualizando(String n1, String n2, String n3, String n4, String n5, String n6, String n7, String[] id)
        {
            int c = 0;
            String[] notas = new string[7];
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            //String Plantilla = @"update notas set ultimamodificacion=now(),valorNota=";
            notas[0] = n1;
            notas[1] = n2;
            notas[2] = n3;
            notas[3] = n4;
            notas[4] = n5;
            notas[5] = n6;
            notas[6] = n7;
            try
            {
                for (int i = 0; i < 7; i++)
                {
                    c += oOperacion.Actualizar(@"update notas set ultimamodificacion=now(),valorNota='" + notas[i] + @"' where idNota='" + id[i] + @"'");
                }
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Error" + ex);
            }
            MessageBox.Show("Numero de notas actualizadas" + c);
        }



        //Carga los elementos de la tabla.
        public void CargarDatos()
        {
            try
            {
                this.dtgvDatos.DataSource = ActualizarTabla(Materia, RadioSeleccionado(), this.grupo); 
            }
            catch
            {

            }
        }
        //Exportar DataGridView a Excel
        public void ExportarExcel()
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = false;
            worksheet = workbook.Sheets["Hoja1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "Reporte Docentes";
            // Cabeceras
            for (int i = 1; i < dtgvDatos.Columns.Count + 1; i++)
            {
                if (i > 1 && i < dtgvDatos.Columns.Count)
                {
                    worksheet.Cells[1, i] = dtgvDatos.Columns[i - 1].HeaderText;
                }
            }
            // Valores
            for (int i = 0; i < dtgvDatos.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dtgvDatos.Columns.Count; j++)
                {
                    if (j > 0 && j < dtgvDatos.Columns.Count - 1)
                    {
                        worksheet.Cells[i + 2, j + 1] = dtgvDatos.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Archivos de Excel|*.xlsx";
            saveFileDialog.Title = "Guardar archivo";
            saveFileDialog.FileName = "NombredeArchivoDefault";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                Console.WriteLine("Ruta en: " + saveFileDialog.FileName);
                workbook.SaveAs(saveFileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                app.Quit();
            }

        }

        private void lblAnio_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            ActualizarGrupos(txtNIP.Text, txtAnio.Text);
            comboBach.SelectedIndex = 0;
            cmbMaterias.SelectedIndex = 0;


        }

        private void comboBach_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBach.Items.Count != 0)
            {
                this.grupo = ListaGrupos[this.comboBach.SelectedIndex].ToString();
                ActualizarMaterias(this.grupo, txtNIP.Text);
                ActualizarTabla(Materia, RadioSeleccionado(), grupo);
            }
            rbt1.Checked = false;
            //MessageBox.Show(grupo);
        }

        private void cmbMaterias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbMaterias.Items.Count != 0)
            {
                this.Materia = ListaMaterias[this.cmbMaterias.SelectedIndex].ToString();
                ActualizarTabla(Materia, Periodo, grupo);
            }
            rbt1.Checked = false;
            //MessageBox.Show(Materia);
        }

        private void txtNIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (CacheManager.SystemCache.Enter(e))
            {
                ActualizarGrupos(txtNIP.Text, txtAnio.Text);
                comboBach.SelectedIndex = 0;
                cmbMaterias.SelectedIndex = 0;
            }
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
         
            CargarDatos();
        }

        private void FrmNotas_Load(object sender, EventArgs e)
        {

        }

        private void dtgvDatos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if(this.dtgvDatos.Columns[e.ColumnIndex].Name == "Promedio")
            {
                try
                {
                    if(e.Value.GetType() != typeof(System.DBNull)) //En caso de haber filas vacias o valores nulos.
                    {
                        if (Convert.ToInt32(e.Value) < 6)
                        {
                            e.CellStyle.BackColor = Color.Red;
                        }
                    }
                }catch(NullReferenceException ex)
                {
                    MessageBox.Show("Error" + ex);
                }
            }
        }

        private void rbt1_CheckedChanged(object sender, EventArgs e)
        {
            //Periodo = 1;
            MessageBox.Show("1");
            CargarDatos();
        }

        private void dtgvDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dtgvDatos.Enabled)
            {
                this.ListaNotas[0] = this.dtgvDatos.CurrentRow.Cells[1].Value.ToString(); //Nombre Alumno.
                this.ListaNotas[1] = this.comboBach.SelectedItem.ToString(); //Nombre Grupo
                this.ListaNotas[2] = this.dtgvDatos.CurrentRow.Cells[13].Value.ToString(); //IDMatriucula.
                this.ListaNotas[3] = ListaMaterias[this.cmbMaterias.SelectedIndex].ToString(); //IDMateria.
                this.ListaNotas[4] = RadioSeleccionado() + ""; //Periodo
                this.ListaNotas[5] = this.dtgvDatos.CurrentRow.Cells[2].Value.ToString(); //Actividad 1
                this.ListaNotas[6] = this.dtgvDatos.CurrentRow.Cells[3].Value.ToString(); //Actividad 2
                this.ListaNotas[7] = this.dtgvDatos.CurrentRow.Cells[4].Value.ToString(); //Actividad 3
                this.ListaNotas[8] = this.dtgvDatos.CurrentRow.Cells[5].Value.ToString(); //Actividad 4
                this.ListaNotas[9] = this.dtgvDatos.CurrentRow.Cells[6].Value.ToString(); //Actividad 5 Auto Evaluacion
                this.ListaNotas[10] = this.dtgvDatos.CurrentRow.Cells[7].Value.ToString(); //Actividad 6 Hetero Evaluacion
                this.ListaNotas[11] = this.dtgvDatos.CurrentRow.Cells[8].Value.ToString(); //Examen Final
                this.ListaNotas[12] = this.cmbMaterias.SelectedItem.ToString(); //Nombre Materia
                this.ListaNotas[13] = this.dtgvDatos.CurrentRow.Cells[12].Value.ToString(); //100% promedio final
            }
            this.ventanaAlumnos = new NotasAlumnos(ListaNotas);
            this.ventanaAlumnos.Show();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void rbt1_CheckedChanged_1(object sender, EventArgs e)
        {
            //MessageBox.Show("hi");
            CargarDatos();
        }

        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            ExportarExcel();
        }

        private void txtNIP_Validated(object sender, EventArgs e)
        {
            if(txtNIP.Text.Trim() == "")
            {
                errp.SetError(txtNIP, "Debe Introducir un NIP");
                txtNIP.Focus();
            }
            else
            {
                errp.Clear();
            }
        }


    }
}
