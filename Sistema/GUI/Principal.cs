﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class Principal : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;

        public Principal()
        {
            InitializeComponent();
        }
        private void comprobarConexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUI.DBPruebas f = new DBPruebas();
            f.Show();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            //lblNombre.Text = _SESION.OUsuario.NombreUsuario;
             //lblUsuario.Text = _SESION.OUsuario.NombreUsuario;
            /*String Rol = _SESION.OUsuario.IDRol;
            int idRol = int.Parse(Rol);
            if (idRol == 1)
            {
                MessageBox.Show("Hola");
            }*/
        }

        private void gestionEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // if (_SESION.OUsuario.VerificarPermiso(2)) {

                General.GUI.GestionEmpleados f = new General.GUI.GestionEmpleados();
                f.MdiParent = this;
                f.Show();
           // }
        }

        private void gestionUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            General.GUI.GestionUsuarios f = new General.GUI.GestionUsuarios();
            f.MdiParent = this;
            f.Show();
        }

        private void toolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void generalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bntNotas_Click(object sender, EventArgs e)
        {
            PANEL_NOTAS.FrmNotas f = new PANEL_NOTAS.FrmNotas();
            f.MdiParent = this.ParentForm;
            f.Show();
            //f.BringToFront();
        }

        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultas_Click(object sender, EventArgs e)
        {
            /*General.GUI.PANEL_CONSULTAS.FrmPrincipal frm = new General.GUI.PANEL_CONSULTAS.FrmPrincipal();
            frm.MdiParent = this.ParentForm;
            frm.Show();*/
        }

        private void bntAlumnos_Click(object sender, EventArgs e)
        {
            General.GUI.PANEL_ALUMNOS.EdicionAlumnos frm = new General.GUI.PANEL_ALUMNOS.EdicionAlumnos();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void btnDocentes_Click(object sender, EventArgs e)
        {
            General.GUI.PANEL_ENCARGADOS.EdicionEncargados frm = new General.GUI.PANEL_ENCARGADOS.EdicionEncargados();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            General.GUI.PANEL_USUARIOS.GestionUsuarios f = new General.GUI.PANEL_USUARIOS.GestionUsuarios();
            f.MdiParent = this.ParentForm;
            f.Show();
        }

        private void statusStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
          
        }

        private void button6_Click(object sender, EventArgs e)
        {
            General.GUI.PANEL_USUARIOS.GestionUsuarios frm = new General.GUI.PANEL_USUARIOS.GestionUsuarios();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            General.GUI.PANEL_ENCARGADOS.EdicionEncargados frm = new General.GUI.PANEL_ENCARGADOS.EdicionEncargados();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            General.GUI.PANEL_DOCENTES.GestionDocentes frm = new General.GUI.PANEL_DOCENTES.GestionDocentes();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
