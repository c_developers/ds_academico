﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class Login : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        //ATRIBUTO
        Boolean _Autorizado = false;
        //PROPIEDAD DE SOLO LECTURA
        public Boolean Autorizado
        {
            get { return _Autorizado; }
            //set { _Autorizado = value; }
        }

        private void Validar(String pUsuario, String pCredencial)
        {
            DataTable DatosUsuario = new DataTable();
            try
            {
                DatosUsuario = CacheManager.SystemCache.ValidarUsuario(pUsuario, pCredencial);
                if (DatosUsuario.Rows.Count == 1)
                {
                    _Autorizado = true;
                    _SESION.OUsuario.IDUsuario = DatosUsuario.Rows[0]["idusuarios"].ToString();
                    _SESION.OUsuario.NombreUsuario = DatosUsuario.Rows[0]["usuario"].ToString();
                    _SESION.OUsuario.Rol = DatosUsuario.Rows[0]["nombrerol"].ToString();
                    _SESION.OUsuario.IDRol = DatosUsuario.Rows[0]["idrol"].ToString();
                    //_SESION.OUsuario.IDDocente = DatosUsuario.Rows[0]["idDocente"].ToString();
                    //_SESION.OUsuario.Docente = DatosUsuario.Rows[0]["nombre"].ToString();

                    Close();
                }
                else
                {
                    _Autorizado = false;
                    MessageBox.Show("Verifique su Usuario/Contraseña","Verificar Credenciales",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                   // lblCredenciales.Text = "USUARIO / CREDENCIAL ERRÓNEOS";
                }
            }
            catch { }
        }

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {

        }

        private void txbCredencial_TextChanged(object sender, EventArgs e)
        {

        }

        private void txbUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txbUsuario_Click(object sender, EventArgs e)
        {
            

        }

        private void txbCredencial_Click(object sender, EventArgs e)
        {
            this.txbCredencial.Text = "";
            this.txbCredencial.ForeColor = System.Drawing.SystemColors.WindowText;
        }

        private void btnEntrar_Click_1(object sender, EventArgs e)
        {
            Validar(txbUsuario.Text, txbCredencial.Text);

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void txbUsuario_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txbCredencial_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txbUsuario_Click_1(object sender, EventArgs e)
        {
            this.txbUsuario.Text = "";
            this.txbUsuario.ForeColor = System.Drawing.SystemColors.WindowText;

        }

        private void txbCredencial_Click_1(object sender, EventArgs e)
        {
            this.txbCredencial.Text = "";
            this.txbCredencial.ForeColor = System.Drawing.SystemColors.WindowText;
        }
    }
}
