﻿namespace Sistema.GUI
{
    partial class DBPruebas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dtgvDatos = new System.Windows.Forms.DataGridView();
            this.txbInsertar = new System.Windows.Forms.TextBox();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.dtgvRoles = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvRoles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(258, 12);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 23);
            this.btnConsultar.TabIndex = 0;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dtgvDatos
            // 
            this.dtgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDatos.Location = new System.Drawing.Point(12, 12);
            this.dtgvDatos.Name = "dtgvDatos";
            this.dtgvDatos.Size = new System.Drawing.Size(240, 150);
            this.dtgvDatos.TabIndex = 1;
            // 
            // txbInsertar
            // 
            this.txbInsertar.Location = new System.Drawing.Point(266, 236);
            this.txbInsertar.Name = "txbInsertar";
            this.txbInsertar.Size = new System.Drawing.Size(140, 20);
            this.txbInsertar.TabIndex = 2;
            // 
            // btnInsertar
            // 
            this.btnInsertar.Location = new System.Drawing.Point(412, 236);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(75, 23);
            this.btnInsertar.TabIndex = 3;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.UseVisualStyleBackColor = true;
            this.btnInsertar.Click += new System.EventHandler(this.btnInsertar_Click);
            // 
            // dtgvRoles
            // 
            this.dtgvRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvRoles.Location = new System.Drawing.Point(12, 236);
            this.dtgvRoles.Name = "dtgvRoles";
            this.dtgvRoles.Size = new System.Drawing.Size(240, 150);
            this.dtgvRoles.TabIndex = 1;
            // 
            // DBPruebas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 418);
            this.Controls.Add(this.btnInsertar);
            this.Controls.Add(this.txbInsertar);
            this.Controls.Add(this.dtgvRoles);
            this.Controls.Add(this.dtgvDatos);
            this.Controls.Add(this.btnConsultar);
            this.Name = "DBPruebas";
            this.Text = "DBPruebas";
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvRoles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DataGridView dtgvDatos;
        private System.Windows.Forms.TextBox txbInsertar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.DataGridView dtgvRoles;
    }
}