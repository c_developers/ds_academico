﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new CLS.AppManager());
            //Application.Run(new General.GUI.PANEL_TITULOS.GestionarTitulos());
            Application.Run(new Sistema.GUI.Principal());
        }
    }
}
