﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Especialidades
    {
        String _idEspecialidad;
        String _nombreEspecialidad;

        public string IdEspecialidad { get => _idEspecialidad; set => _idEspecialidad = value; }
        public string NombreEspecialidad { get => _nombreEspecialidad; set => _nombreEspecialidad = value; }

        public Boolean Guardar()
        {

            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into especialidadBachillerato(NombreEsp) values(");
            Sentencia.Append("'" + _nombreEspecialidad + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
