﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Encargados
    {
        String _IDEncargado;
        String _Nombres;
        String _Apellidos;
        String _Telefono;
        String _DUI;
        String _Trabajo;

        public string Nombres { get => _Nombres; set => _Nombres = value; }
        public string Apellidos { get => _Apellidos; set => _Apellidos = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
        public string DUI { get => _DUI; set => _DUI = value; }
        public string Trabajo { get => _Trabajo; set => _Trabajo = value; }
        public string IDEncargado { get => _IDEncargado; set => _IDEncargado = value; }

        public Boolean Guardar()
        {

            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into encargados( nombres_encargado, apellidos_encargado, telefono, DUI, trabajo) values(");
          //  Sentencia.Append("'" + _IDEncargado + "',");
            Sentencia.Append("'" + _Nombres + "',");
            Sentencia.Append("'" + _Apellidos + "',");
            Sentencia.Append("'" + _Telefono + "',");
            Sentencia.Append("'" + _DUI + "',");
            Sentencia.Append("'" + _Trabajo + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE encargados SET ");
            Sentencia.Append("nombres_encargado='" + Nombres + "',");
            Sentencia.Append("apellidos_encargado='" + Apellidos + "',");
            Sentencia.Append("telefono='" + Telefono + "',");
            Sentencia.Append("DUI='" + DUI + "',");
            Sentencia.Append("trabajo='" + _Trabajo + "' WHERE idEncargado='" + _IDEncargado + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM encargados ");
            Sentencia.Append("WHERE idEncargado='" + _IDEncargado + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
    }   

