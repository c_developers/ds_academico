﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Empleado
    {
        String _IDEmpleado;
        String _Nombres;
        String _Apellidos;
        String _FechaNacimiento;
        String _DUI;
        String _NIT;
        String _Telefono;
        String _Direccion;

        public String Direccion
        {
            get { return _Direccion; }
            set { _Direccion = value; }
        }

        public String Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        public String NIT
        {
            get { return _NIT; }
            set { _NIT = value; }
        }

        public String DUI
        {
            get { return _DUI; }
            set { _DUI = value; }
        }

        public String FechaNacimiento
        {
            get { return _FechaNacimiento; }
            set { _FechaNacimiento = value; }
        }

        public String Apellidos
        {
            get { return _Apellidos; }
            set { _Apellidos = value; }
        }

        public String Nombres
        {
            get { return _Nombres; }
            set { _Nombres = value; }
        }

        public String IDEmpleado
        {
            get { return _IDEmpleado; }
            set { _IDEmpleado = value; }
        }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into empleados(Nombres, Apellidos, FechaNacimiento, DUI, NIT, Telefono, Direccion) values(");
            Sentencia.Append("'" + _Nombres + "',");
            Sentencia.Append("'" + _Apellidos + "',");
            Sentencia.Append("'" + _FechaNacimiento + "',");
            Sentencia.Append("'" + _DUI + "',");
            Sentencia.Append("'" + _NIT + "',");
            Sentencia.Append("'" + _Telefono + "',");
            Sentencia.Append("'" + _Direccion + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if(oOperacion.Insertar(Sentencia.ToString()) > 0){
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE empleados SET ");
            Sentencia.Append("Nombres='" + _Nombres + "',");
            Sentencia.Append("Apellidos='" + _Apellidos + "',");
            Sentencia.Append("FechaNacimiento='" + _FechaNacimiento + "',");
            Sentencia.Append("DUI='" + _DUI + "',");
            Sentencia.Append("NIT='" + _NIT + "',");
            Sentencia.Append("Telefono='" + _Telefono + "',");
            Sentencia.Append("Direccion='" + _Direccion + "' WHERE IDEmpleado='"+ _IDEmpleado+ "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM empleados ");
            Sentencia.Append("WHERE IDEmpleado='" + _IDEmpleado + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
