﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Alumnos
    {
        String _IDAlumno;
        String _PrimerNombre;
        String _SegundoNombre;
        String _PrimerApellido;
        String _SegundoApellido;
        String _idDepartamento;
        String _idMunicipio;
        String _zona;
        String _Direccion;
        String _Calle;
        String _FechaNacimiento;
        String _AlergiasEnfermedades;
        int _estatus;
        String _estatusDescripcion;
        String _anioInicio;
        String _anioGraduacion;

        public string IDAlumno { get => _IDAlumno; set => _IDAlumno = value; }
        public string PrimerNombre { get => _PrimerNombre; set => _PrimerNombre = value; }
        public string SegundoNombre { get => _SegundoNombre; set => _SegundoNombre = value; }
        public string PrimerApellido { get => _PrimerApellido; set => _PrimerApellido = value; }
        public string SegundoApellido { get => _SegundoApellido; set => _SegundoApellido = value; }
        public string IdDepartamento { get => _idDepartamento; set => _idDepartamento = value; }
        public string IdMunicipio { get => _idMunicipio; set => _idMunicipio = value; }
        public string Zona { get => _zona; set => _zona = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Calle { get => _Calle; set => _Calle = value; }
        public string FechaNacimiento { get => _FechaNacimiento; set => _FechaNacimiento = value; }
        public string AlergiasEnfermedades { get => _AlergiasEnfermedades; set => _AlergiasEnfermedades = value; }
        public int Estatus { get => _estatus; set => _estatus = value; }
        public string EstatusDescripcion { get => _estatusDescripcion; set => _estatusDescripcion = value; }
        public string AnioInicio { get => _anioInicio; set => _anioInicio = value; }
        public string AnioGraduacion { get => _anioGraduacion; set => _anioGraduacion = value; }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into alumnos(NIE, PNombre, SNombre, Papellido, Sapellido, idDepartamento, idMunicipio, zonaUR, direccion, calle, fechNacimiento, alergias_enfermedades, estatus, estatus_descripcion, anioInicio, anioGraducion) values(");
            Sentencia.Append("'" + _IDAlumno + "',");
            Sentencia.Append("'" + _PrimerNombre + "',");
            Sentencia.Append("'" + _SegundoNombre + "',");
            Sentencia.Append("'" + _PrimerApellido + "',");
            Sentencia.Append("'" + _SegundoApellido + "',");
            Sentencia.Append("'" + _idDepartamento + "',");
            Sentencia.Append("'" + _idMunicipio + "',");
            Sentencia.Append("'" + _zona + "',");
            Sentencia.Append("'" + _Direccion + "',");
            Sentencia.Append("'" + _Calle + "',");
            Sentencia.Append("'" + _FechaNacimiento + "',");
            Sentencia.Append("'" + _AlergiasEnfermedades + "',");
            Sentencia.Append("'" + _estatus + "',");
            Sentencia.Append("'" + _estatusDescripcion + "',");
            Sentencia.Append("'" + _anioInicio + "',");
            Sentencia.Append("null);");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch (Exception err)
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE alumnos SET ");
            Sentencia.Append("'" + _PrimerNombre + "',");
            Sentencia.Append("'" + _SegundoNombre + "',");
            Sentencia.Append("'" + _PrimerApellido + "',");
            Sentencia.Append("'" + _SegundoApellido + "',");
            Sentencia.Append("'" + _idDepartamento + "',");
            Sentencia.Append("'" + _idMunicipio + "',");
            Sentencia.Append("'" + _zona + "',");
            Sentencia.Append("'" + _Direccion + "',");
            Sentencia.Append("'" + _Calle + "',");
            Sentencia.Append("'" + _FechaNacimiento + "',");
            Sentencia.Append("'" + _AlergiasEnfermedades + "',");
            Sentencia.Append("'" + _estatus + "',");
            Sentencia.Append("'" + _estatusDescripcion + "',");
            Sentencia.Append("'" + _anioInicio + "',");
            Sentencia.Append("null WHERE NIE=" + _IDAlumno + ";");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

    }
}
