﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Usuario
    {
        String _IDUsuario;
        String _Usuarios;
        String _Credencial;
        String _IDDocente;
        String _IDRol;

        public string IDUsuario { get => _IDUsuario; set => _IDUsuario = value; }
        public string Usuarios { get => _Usuarios; set => _Usuarios = value; }
        public string Credencial { get => _Credencial; set => _Credencial = value; }
        public string IDDocente { get => _IDDocente; set => _IDDocente = value; }
        public string IDRol { get => _IDRol; set => _IDRol = value; }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into usuarios(usuario, contrasenia, idDocente, idRol) values(");
            Sentencia.Append("'" + _Usuarios + "',");
            Sentencia.Append("sha1('" + _Credencial + "'),");
            Sentencia.Append("'" + _IDDocente + "',");
            Sentencia.Append("'" + _IDRol + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean GuardarUsuario()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into usuarios(usuario, contrasenia, idDocente, idRol) values(");
            Sentencia.Append("'" + _Usuarios + "',");
            Sentencia.Append("sha1('" + _Credencial + "'),");
            Sentencia.Append(" null ,");
            Sentencia.Append("'" + _IDRol + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;

        }
        public Boolean ActualizarUsuario()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE usuarios SET ");
            Sentencia.Append("usuario='" + _Usuarios + "',");
            Sentencia.Append("contrasenia=sha1('" + _Credencial + "'),");
            Sentencia.Append("idDocente= null,");
            Sentencia.Append("idRol='" + _IDRol + "' WHERE idUsuarios='" + _IDUsuario + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE usuarios SET ");
            Sentencia.Append("usuario='" + _Usuarios + "',");
            Sentencia.Append("contrasenia=sha1('" + _Credencial + "'),");
            Sentencia.Append("idDocente='" + _IDDocente + "',");
            Sentencia.Append("idRol='" + _IDRol + "' WHERE idUsuarios='" + _IDUsuario + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM usuarios ");
            Sentencia.Append("WHERE idUsuarios='" + _IDUsuario + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
