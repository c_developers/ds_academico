﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Docentes
    {
        String _idDocente;
        String _NIDocente;
        String _NUP;
        String _Nombre;
        String _Apellido;
        String _DUI;
        String _NIT;
        String _telefono;
        String _Direccion;
        int _laborando;
        String _idTitulo;

        public string IdDocente { get => _idDocente; set => _idDocente = value; }
        public string NIDocente { get => _NIDocente; set => _NIDocente = value; }
        public string NUP { get => _NUP; set => _NUP = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Apellido { get => _Apellido; set => _Apellido = value; }
        public string DUI { get => _DUI; set => _DUI = value; }
        public string NIT { get => _NIT; set => _NIT = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string IdTitulo { get => _idTitulo; set => _idTitulo = value; }
        public int Laborando { get => _laborando; set => _laborando = value; }

        public Boolean Guardar()
        {

            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into Docentes(NIDocente, NUP, nombre , apellido, DUI, NIT, telefono, direccion, laborando, idTitulo) values(");
            Sentencia.Append("'" + _NIDocente  + "',");
            Sentencia.Append("'" + _NUP + "',");
            Sentencia.Append("'" + _Nombre + "',");
            Sentencia.Append("'" + _Apellido + "',");
            Sentencia.Append("'" + _DUI + "',");
            Sentencia.Append("'" + _NIT + "',");
            Sentencia.Append("'" + _telefono + "',");
            Sentencia.Append("'" + _Direccion + "',");
            Sentencia.Append("'" + _laborando + "',");
            Sentencia.Append("'" + _idTitulo + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE docentes SET ");
            Sentencia.Append("idDocente='" + _idDocente + "',");
            Sentencia.Append("NIDocente='" + _NIDocente + "',");
            Sentencia.Append("NUP='" + _NUP + "',");
            Sentencia.Append("nombre='" + _Nombre + "',");
            Sentencia.Append("apellido='" + _Apellido + "',");
            Sentencia.Append("DUI='" + _DUI + "',");
            Sentencia.Append("NIT='" + _NIT + "',");
            Sentencia.Append("telefono='" + _telefono + "',");
            Sentencia.Append("direccion='" + _Direccion + "',");
            Sentencia.Append("laborando='" + _laborando + "',");
            Sentencia.Append("idTitulo='" + _idTitulo + "' WHERE idDocente='" + _idDocente + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM docentes ");
            Sentencia.Append("WHERE idDocente='" + _idDocente + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
