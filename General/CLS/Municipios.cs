﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Municipios
    {
        String _idMunicipio;
        String _Municipio;
        String _idDepartamento;

        public string IdMunicipio { get => _idMunicipio; set => _idMunicipio = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }
        public string IdDepartamento { get => _idDepartamento; set => _idDepartamento = value; }

        public Boolean Guardar()
        {

            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into municipios( municipio, idDepartamento) values(");
            Sentencia.Append("'" + _Municipio + "',");
            Sentencia.Append("'" + _idDepartamento + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE municipios SET ");
            Sentencia.Append("municipio='" + _Municipio + "' WHERE idMunicipio='" + _idMunicipio + "';");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
