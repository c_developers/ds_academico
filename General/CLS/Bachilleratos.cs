﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Bachilleratos
    {
        String _idBachillerato;
        String _nombreResultante;
        String _anioLectivo;
        String _TipoBachillerato;
        String _idEspecialidadBach;

        public string IdBachillerato { get => _idBachillerato; set => _idBachillerato = value; }
        public string NombreResultante { get => _nombreResultante; set => _nombreResultante = value; }
        public string AnioLectivo { get => _anioLectivo; set => _anioLectivo = value; }
        public string TipoBachillerato { get => _TipoBachillerato; set => _TipoBachillerato = value; }
        public string IdEspecialidadBach { get => _idEspecialidadBach; set => _idEspecialidadBach = value; }

        public Boolean Guardar()
        {

            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into bachilleratos(nombreResultante, anioLectivo, TipoBachillerato, idEspecialidadBach) values(");
            Sentencia.Append("'" + _nombreResultante + "',");
            Sentencia.Append("'" + _anioLectivo + "',");
            Sentencia.Append("'" + _TipoBachillerato + "',");
            Sentencia.Append("'" + _idEspecialidadBach + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
