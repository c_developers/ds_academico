﻿namespace General.GUI
{
    partial class EdicionUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txbIDUsuario = new System.Windows.Forms.TextBox();
            this.txbUsuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.Notificador = new System.Windows.Forms.ErrorProvider(this.components);
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.btnElegirEmpleado = new System.Windows.Forms.Button();
            this.txbEmpleado = new System.Windows.Forms.TextBox();
            this.lblIDEmpleado = new System.Windows.Forms.Label();
            this.lblIDRol = new System.Windows.Forms.Label();
            this.btnElegirRol = new System.Windows.Forms.Button();
            this.txbRol = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IDUsuario";
            // 
            // txbIDUsuario
            // 
            this.txbIDUsuario.Location = new System.Drawing.Point(33, 46);
            this.txbIDUsuario.Name = "txbIDUsuario";
            this.txbIDUsuario.ReadOnly = true;
            this.txbIDUsuario.Size = new System.Drawing.Size(58, 20);
            this.txbIDUsuario.TabIndex = 1;
            // 
            // txbUsuario
            // 
            this.txbUsuario.Location = new System.Drawing.Point(97, 46);
            this.txbUsuario.Name = "txbUsuario";
            this.txbUsuario.Size = new System.Drawing.Size(150, 20);
            this.txbUsuario.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Usuario";
            // 
            // txbPass
            // 
            this.txbPass.Location = new System.Drawing.Point(30, 96);
            this.txbPass.Name = "txbPass";
            this.txbPass.Size = new System.Drawing.Size(217, 20);
            this.txbPass.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Contraseña";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Empleado";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Rol";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Estado";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(91, 291);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 12;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            //this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(172, 291);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            //this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // Notificador
            // 
            this.Notificador.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.Notificador.ContainerControl = this;
            // 
            // cmbEstado
            // 
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Items.AddRange(new object[] {
            "ACTIVADO",
            "BLOQUEADO"});
            this.cmbEstado.Location = new System.Drawing.Point(30, 245);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(217, 21);
            this.cmbEstado.TabIndex = 11;
            // 
            // btnElegirEmpleado
            // 
            this.btnElegirEmpleado.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElegirEmpleado.Location = new System.Drawing.Point(209, 145);
            this.btnElegirEmpleado.Name = "btnElegirEmpleado";
            this.btnElegirEmpleado.Size = new System.Drawing.Size(38, 20);
            this.btnElegirEmpleado.TabIndex = 19;
            this.btnElegirEmpleado.Text = "...";
            this.btnElegirEmpleado.UseVisualStyleBackColor = true;
           // this.btnElegirEmpleado.Click += new System.EventHandler(this.btnElegirEmpleado_Click);
            // 
            // txbEmpleado
            // 
            this.txbEmpleado.Location = new System.Drawing.Point(30, 145);
            this.txbEmpleado.Name = "txbEmpleado";
            this.txbEmpleado.ReadOnly = true;
            this.txbEmpleado.Size = new System.Drawing.Size(173, 20);
            this.txbEmpleado.TabIndex = 18;
            // 
            // lblIDEmpleado
            // 
            this.lblIDEmpleado.AutoSize = true;
            this.lblIDEmpleado.Location = new System.Drawing.Point(100, 129);
            this.lblIDEmpleado.Name = "lblIDEmpleado";
            this.lblIDEmpleado.Size = new System.Drawing.Size(0, 13);
            this.lblIDEmpleado.TabIndex = 22;
            this.lblIDEmpleado.Visible = false;
            // 
            // lblIDRol
            // 
            this.lblIDRol.AutoSize = true;
            this.lblIDRol.Location = new System.Drawing.Point(100, 178);
            this.lblIDRol.Name = "lblIDRol";
            this.lblIDRol.Size = new System.Drawing.Size(0, 13);
            this.lblIDRol.TabIndex = 23;
            this.lblIDRol.Visible = false;
            // 
            // btnElegirRol
            // 
            this.btnElegirRol.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElegirRol.Location = new System.Drawing.Point(209, 195);
            this.btnElegirRol.Name = "btnElegirRol";
            this.btnElegirRol.Size = new System.Drawing.Size(38, 20);
            this.btnElegirRol.TabIndex = 26;
            this.btnElegirRol.Text = "...";
            this.btnElegirRol.UseVisualStyleBackColor = true;
           // this.btnElegirRol.Click += new System.EventHandler(this.btnElegirRol_Click);
            // 
            // txbRol
            // 
            this.txbRol.Location = new System.Drawing.Point(30, 195);
            this.txbRol.Name = "txbRol";
            this.txbRol.ReadOnly = true;
            this.txbRol.Size = new System.Drawing.Size(173, 20);
            this.txbRol.TabIndex = 25;
            // 
            // EdicionUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 343);
            this.Controls.Add(this.btnElegirRol);
            this.Controls.Add(this.txbRol);
            this.Controls.Add(this.lblIDRol);
            this.Controls.Add(this.lblIDEmpleado);
            this.Controls.Add(this.btnElegirEmpleado);
            this.Controls.Add(this.txbEmpleado);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.cmbEstado);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txbPass);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txbUsuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txbIDUsuario);
            this.Controls.Add(this.label1);
            this.Name = "EdicionUsuarios";
            this.Text = "EdicionUsuarios";
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        public System.Windows.Forms.TextBox txbIDUsuario;
        public System.Windows.Forms.TextBox txbUsuario;
        public System.Windows.Forms.TextBox txbPass;
        private System.Windows.Forms.ErrorProvider Notificador;
        private System.Windows.Forms.Button btnElegirEmpleado;
        public System.Windows.Forms.TextBox txbEmpleado;
        public System.Windows.Forms.ComboBox cmbEstado;
        public System.Windows.Forms.Label lblIDEmpleado;
        public System.Windows.Forms.Label lblIDRol;
        private System.Windows.Forms.Button btnElegirRol;
        public System.Windows.Forms.TextBox txbRol;
    }
}