﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_BACHILLERATOS
{
    public partial class EdicionBachilleratos : Form
    {
        BindingSource _EspecialidadBachilleratos = new BindingSource();

       
        public void TipoBachillerato()
        {
            try
            {
                this.cmbTipoBachillerato.Items.Add("GENERAL");
                this.cmbTipoBachillerato.Items.Add("TECNICO VOCACIONAL COMERCIAL");
                this.cmbTipoBachillerato.SelectedIndex = 0;
            }
            catch(Exception err) { }
        }
        public void EspecialidadBachillerato()
        {
            try
            {
                this._EspecialidadBachilleratos.DataSource = CacheManager.SystemCache.ESPECIALIDADESBACHILLERATOS();
                this.cmbEspecialidad.DataSource = _EspecialidadBachilleratos;
                this.cmbEspecialidad.DisplayMember = "NombreEsp";
                this.cmbEspecialidad.ValueMember = "idEspecialidadBach";
                this.cmbEspecialidad.SelectedIndex = 0;
            }
            catch (Exception err) { }
        }

        public void Procesar()
        {

            CLS.Bachilleratos Bach = new CLS.Bachilleratos();
            Bach.IdBachillerato = txtIDBACH.Text;
            Bach.AnioLectivo = cmbAnioLectivo.SelectedItem.ToString();
            Bach.TipoBachillerato = cmbTipoBachillerato.SelectedItem.ToString();
            Bach.IdEspecialidadBach = cmbEspecialidad.SelectedValue.ToString();
            String espe = cmbEspecialidad.SelectedValue.ToString();
            String anio = cmbAnioLectivo.SelectedItem.ToString();
            String Tipo = cmbTipoBachillerato.SelectedItem.ToString();
            String atributo = "OPCION";
            if ("GENERAL".Equals(Tipo))
            {

                String Resultado = anio + " " + "BACHILLERATO GENERAl";
                atributo = "DIPLOMADO EN";
                Bach.NombreResultante = Resultado;
            }
            else
            {
                if("TECNICO VOCACIONAL COMERCIAL".Equals(Tipo))
                {
                    String Resultado = anio + " " + "BACHILLERATO TECNICO VOCACIONAL";
                    atributo = "DIPLOMADO EN";
                    Bach.NombreResultante = Resultado;
                }
            }


            if (txtIDBACH.TextLength == 0)
                {
                    //Estoy insertando un nuevo regitro
                    if (Bach.Guardar())
                    {
                        MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("No pude Insertar");
                    }
            }
        }
        public void AnioLectivo()
        {
        }

        public EdicionBachilleratos()
        {
            InitializeComponent();
        }

        private void EdicionBachilleratos_Load(object sender, EventArgs e)
        {
            AnioLectivo();
            TipoBachillerato();
            EspecialidadBachillerato();
        }

        private void btnNuevaEspecialidad_Click(object sender, EventArgs e)
        {
            PANEL_ESPECIALIDADES.EdicionEspecialidad frm = new PANEL_ESPECIALIDADES.EdicionEspecialidad();
            frm.ShowDialog();
            EspecialidadBachillerato();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void cmbTipoBachillerato_SelectedIndexChanged(object sender, EventArgs e)
        {
            String ValorTipo = this.cmbTipoBachillerato.SelectedItem.ToString();
            if (ValorTipo == "GENERAL")
            {
                this.cmbAnioLectivo.Items.Clear();
                this.cmbAnioLectivo.Items.Add("1°");
                this.cmbAnioLectivo.Items.Add("2°");
                this.cmbAnioLectivo.SelectedIndex = 0;
            }
            else
            {
                this.cmbAnioLectivo.Items.Clear();
                this.cmbAnioLectivo.Items.Add("1°");
                this.cmbAnioLectivo.Items.Add("2°");
                this.cmbAnioLectivo.Items.Add("3°");
            }
        }
    }
}
