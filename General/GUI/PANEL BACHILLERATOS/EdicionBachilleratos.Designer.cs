﻿namespace General.GUI.PANEL_BACHILLERATOS
{
    partial class EdicionBachilleratos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblAnioLectivo = new System.Windows.Forms.Label();
            this.lbTipoBachillerato = new System.Windows.Forms.Label();
            this.cmbAnioLectivo = new System.Windows.Forms.ComboBox();
            this.cmbTipoBachillerato = new System.Windows.Forms.ComboBox();
            this.lblEspecialidad = new System.Windows.Forms.Label();
            this.cmbEspecialidad = new System.Windows.Forms.ComboBox();
            this.btnNuevaEspecialidad = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.lblIDBach = new System.Windows.Forms.Label();
            this.txtIDBACH = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(251, 35);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(188, 13);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "INGRESAR NUEVO BACHILLERATO";
            // 
            // lblAnioLectivo
            // 
            this.lblAnioLectivo.AutoSize = true;
            this.lblAnioLectivo.Location = new System.Drawing.Point(52, 76);
            this.lblAnioLectivo.Name = "lblAnioLectivo";
            this.lblAnioLectivo.Size = new System.Drawing.Size(66, 13);
            this.lblAnioLectivo.TabIndex = 1;
            this.lblAnioLectivo.Text = "Anio Lectivo";
            // 
            // lbTipoBachillerato
            // 
            this.lbTipoBachillerato.AutoSize = true;
            this.lbTipoBachillerato.Location = new System.Drawing.Point(373, 76);
            this.lbTipoBachillerato.Name = "lbTipoBachillerato";
            this.lbTipoBachillerato.Size = new System.Drawing.Size(86, 13);
            this.lbTipoBachillerato.TabIndex = 2;
            this.lbTipoBachillerato.Text = "Tipo Bachillerato";
            // 
            // cmbAnioLectivo
            // 
            this.cmbAnioLectivo.FormattingEnabled = true;
            this.cmbAnioLectivo.Location = new System.Drawing.Point(55, 92);
            this.cmbAnioLectivo.Name = "cmbAnioLectivo";
            this.cmbAnioLectivo.Size = new System.Drawing.Size(63, 21);
            this.cmbAnioLectivo.TabIndex = 3;
            // 
            // cmbTipoBachillerato
            // 
            this.cmbTipoBachillerato.FormattingEnabled = true;
            this.cmbTipoBachillerato.Location = new System.Drawing.Point(376, 92);
            this.cmbTipoBachillerato.Name = "cmbTipoBachillerato";
            this.cmbTipoBachillerato.Size = new System.Drawing.Size(263, 21);
            this.cmbTipoBachillerato.TabIndex = 4;
            this.cmbTipoBachillerato.SelectedIndexChanged += new System.EventHandler(this.cmbTipoBachillerato_SelectedIndexChanged);
            // 
            // lblEspecialidad
            // 
            this.lblEspecialidad.AutoSize = true;
            this.lblEspecialidad.Location = new System.Drawing.Point(52, 179);
            this.lblEspecialidad.Name = "lblEspecialidad";
            this.lblEspecialidad.Size = new System.Drawing.Size(125, 13);
            this.lblEspecialidad.TabIndex = 5;
            this.lblEspecialidad.Text = "Especialidad Bachillerato";
            // 
            // cmbEspecialidad
            // 
            this.cmbEspecialidad.FormattingEnabled = true;
            this.cmbEspecialidad.Location = new System.Drawing.Point(55, 195);
            this.cmbEspecialidad.Name = "cmbEspecialidad";
            this.cmbEspecialidad.Size = new System.Drawing.Size(347, 21);
            this.cmbEspecialidad.TabIndex = 6;
            // 
            // btnNuevaEspecialidad
            // 
            this.btnNuevaEspecialidad.Location = new System.Drawing.Point(427, 189);
            this.btnNuevaEspecialidad.Name = "btnNuevaEspecialidad";
            this.btnNuevaEspecialidad.Size = new System.Drawing.Size(252, 31);
            this.btnNuevaEspecialidad.TabIndex = 7;
            this.btnNuevaEspecialidad.Text = "Insertar Nueva Especialidad";
            this.btnNuevaEspecialidad.UseVisualStyleBackColor = true;
            this.btnNuevaEspecialidad.Click += new System.EventHandler(this.btnNuevaEspecialidad_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(528, 294);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(151, 52);
            this.btnConfirmar.TabIndex = 8;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // lblIDBach
            // 
            this.lblIDBach.AutoSize = true;
            this.lblIDBach.Enabled = false;
            this.lblIDBach.Location = new System.Drawing.Point(52, 18);
            this.lblIDBach.Name = "lblIDBach";
            this.lblIDBach.Size = new System.Drawing.Size(49, 13);
            this.lblIDBach.TabIndex = 9;
            this.lblIDBach.Text = "ID Bach:";
            this.lblIDBach.Visible = false;
            // 
            // txtIDBACH
            // 
            this.txtIDBACH.Enabled = false;
            this.txtIDBACH.Location = new System.Drawing.Point(55, 35);
            this.txtIDBACH.Name = "txtIDBACH";
            this.txtIDBACH.Size = new System.Drawing.Size(46, 20);
            this.txtIDBACH.TabIndex = 10;
            this.txtIDBACH.Visible = false;
            // 
            // EdicionBachilleratos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 370);
            this.Controls.Add(this.txtIDBACH);
            this.Controls.Add(this.lblIDBach);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.btnNuevaEspecialidad);
            this.Controls.Add(this.cmbEspecialidad);
            this.Controls.Add(this.lblEspecialidad);
            this.Controls.Add(this.cmbTipoBachillerato);
            this.Controls.Add(this.cmbAnioLectivo);
            this.Controls.Add(this.lbTipoBachillerato);
            this.Controls.Add(this.lblAnioLectivo);
            this.Controls.Add(this.lblTitulo);
            this.Name = "EdicionBachilleratos";
            this.Text = "EdicionBachilleratos";
            this.Load += new System.EventHandler(this.EdicionBachilleratos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnConfirmar;
        public System.Windows.Forms.Label lblAnioLectivo;
        public System.Windows.Forms.Label lbTipoBachillerato;
        public System.Windows.Forms.ComboBox cmbAnioLectivo;
        public System.Windows.Forms.ComboBox cmbTipoBachillerato;
        public System.Windows.Forms.Label lblEspecialidad;
        public System.Windows.Forms.ComboBox cmbEspecialidad;
        public System.Windows.Forms.Button btnNuevaEspecialidad;
        public System.Windows.Forms.Label lblIDBach;
        private System.Windows.Forms.TextBox txtIDBACH;
    }
}