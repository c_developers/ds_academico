﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EleccionDetalleEmpleado : Form
    {
        BindingSource _Empleados = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                _Empleados.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Empleados.Filter = "Nombres LIKE '%" + txtFiltro.Text + "%' OR Apellidos like '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Empleados.RemoveFilter();
                }
                dtgvDatos.AutoGenerateColumns = false;
                dtgvDatos.DataSource = _Empleados;
                lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        public EleccionDetalleEmpleado()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void EleccionDetalleEmpleado_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            EdicionUsuarios f = Owner as EdicionUsuarios;

            //Sincronizando interfaz gráfica con registro seleccionado
            f.lblIDEmpleado.Text = dtgvDatos.CurrentRow.Cells["IDEmpleado"].Value.ToString();
            f.txbEmpleado.Text = dtgvDatos.CurrentRow.Cells["Nombres"].Value.ToString() + " " + dtgvDatos.CurrentRow.Cells["Apellidos"].Value.ToString();
            Close();
        }

        private void dtgvDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EdicionUsuarios f = Owner as EdicionUsuarios;

            //Sincronizando interfaz gráfica con registro seleccionado
            f.lblIDEmpleado.Text = dtgvDatos.CurrentRow.Cells["IDEmpleado"].Value.ToString();
            f.txbEmpleado.Text = dtgvDatos.CurrentRow.Cells["Nombres"].Value.ToString() + " " + dtgvDatos.CurrentRow.Cells["Apellidos"].Value.ToString();            
            Close();
        }
    }
}
