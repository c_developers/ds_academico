﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionEmpleados : Form
    {
        BindingSource _Empleados = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                _Empleados.DataSource = CacheManager.SystemCache.TODOSLOSEMPLEADOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Empleados.Filter = "Nombres LIKE '%" + txtFiltro.Text + "%' OR Apellidos like '%" + txtFiltro.Text + "%'";
                }
                else{
                    _Empleados.RemoveFilter();
                }
                dtgvDatos.AutoGenerateColumns = false;
                dtgvDatos.DataSource = _Empleados;
                lblRegistros.Text = dtgvDatos.Rows.Count.ToString()+ " Registro encontrados";
            }
            catch
            {

            }
        }

        public GestionEmpleados()
        {
            InitializeComponent();
        }

        private void GestionEmpleados_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionEmpleados f = new EdicionEmpleados();
            f.ShowDialog();
            CargarDatos();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionEmpleados f = new EdicionEmpleados();

                //Sincronizando interfaz gráfica con registro seleccionado
                //(Muestra los datos del registro a editar)
                f.txbIDEmpleado.Text = dtgvDatos.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                f.txbNombres.Text = dtgvDatos.CurrentRow.Cells["Nombres"].Value.ToString();
                f.txbApellidos.Text = dtgvDatos.CurrentRow.Cells["Apellidos"].Value.ToString();
                f.dtpFecha.Text = dtgvDatos.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
                f.txbDireccion.Text = dtgvDatos.CurrentRow.Cells["Direccion"].Value.ToString();
                f.txbDUI.Text = dtgvDatos.CurrentRow.Cells["DUI"].Value.ToString();
                f.txbNIT.Text = dtgvDatos.CurrentRow.Cells["NIT"].Value.ToString();
                f.txbTelefono.Text = dtgvDatos.CurrentRow.Cells["Telefono"].Value.ToString();
                f.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Empleado oEmpleado = new CLS.Empleado();
                oEmpleado.IDEmpleado = dtgvDatos.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                if (oEmpleado.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos(); 
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
