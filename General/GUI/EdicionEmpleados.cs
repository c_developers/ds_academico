﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionEmpleados : Form
    {
        public void Procesar()
        {
            CLS.Empleado oEmpleado = new CLS.Empleado();
            oEmpleado.IDEmpleado = txbIDEmpleado.Text;
            oEmpleado.Nombres = txbNombres.Text;
            oEmpleado.Apellidos = txbApellidos.Text;
            oEmpleado.FechaNacimiento = dtpFecha.Text;
            oEmpleado.DUI = txbDUI.Text;
            oEmpleado.NIT = txbNIT.Text;
            oEmpleado.Telefono = txbTelefono.Text;
            oEmpleado.Direccion = txbDireccion.Text;
            if (txbIDEmpleado.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oEmpleado.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (oEmpleado.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txbNombres.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbNombres, "Este campo no puede quedar vacío");
            }

            if (txbApellidos.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbApellidos, "Este campo no puede quedar vacío");
            }

            if (dtpFecha.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpFecha, "Este campo no puede quedar vacío");
            }


            if (txbNIT.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbNIT, "Este campo no puede quedar vacío");
            }

            if (txbDUI.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDUI, "Este campo no puede quedar vacío");
            }

            if (txbTelefono.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbTelefono, "Este campo no puede quedar vacío");
            }

            if (txbDireccion.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDireccion, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }

        public EdicionEmpleados()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
