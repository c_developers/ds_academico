﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_TITULOS
{
    public partial class EditarTitulos : Form
    {
        BindingSource _Titulos = new BindingSource();

        public void Procesar()
        {
            CLS.Titulos titulo = new CLS.Titulos();
            titulo.IdTitulo = txtIDTitulo.Text;
            titulo.NombreCarrera = txtNombreCarrera.Text;
            if(txtIDTitulo.TextLength == 0)
            {
                if (titulo.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (titulo.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public EditarTitulos()
        {
            InitializeComponent();
        }

        private void btnAgregarTitulo_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblTitulo_Click(object sender, EventArgs e)
        {

        }
    }
}
