﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_TITULOS
{
    public partial class GestionarTitulos : Form
    {
        BindingSource _Titulos = new BindingSource();
        public void CargarDatos()
        {
            this._Titulos.DataSource = CacheManager.SystemCache.Titulos();
            this.dtgvTitulos.DataSource = _Titulos;
        }
        public GestionarTitulos()
        {
            InitializeComponent();
        }

        private void GestionarTitulos_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EditarTitulos frm = new EditarTitulos();
            frm.ShowDialog();
            CargarDatos();
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltrar.TextLength > 0)
                {
                    _Titulos.Filter = "nombreCarrera LIKE '%" + txtFiltrar.Text +  "%'";
                }
                else
                {
                    _Titulos.RemoveFilter();
                }
                dtgvTitulos.AutoGenerateColumns = false;
                dtgvTitulos.DataSource = _Titulos;
                //lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }
        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EditarTitulos frmEditar = new EditarTitulos();
                frmEditar.txtIDTitulo.Text = dtgvTitulos.CurrentRow.Cells["idTitulo"].Value.ToString();
                frmEditar.txtNombreCarrera.Text = dtgvTitulos.CurrentRow.Cells["nombreCarrera"].Value.ToString();
                frmEditar.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Titulos title = new CLS.Titulos();
                title.IdTitulo = dtgvTitulos.CurrentRow.Cells["idTitulo"].Value.ToString();
                if (title.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            EditarTitulos frm = new EditarTitulos();
            frm.ShowDialog();
            CargarDatos();
        }

        private void btnEditar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EditarTitulos frmEditar = new EditarTitulos();
                frmEditar.txtIDTitulo.Text = dtgvTitulos.CurrentRow.Cells["idTitulo"].Value.ToString();
                frmEditar.txtNombreCarrera.Text = dtgvTitulos.CurrentRow.Cells["nombreCarrera"].Value.ToString();
                frmEditar.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Titulos title = new CLS.Titulos();
                title.IdTitulo = dtgvTitulos.CurrentRow.Cells["idTitulo"].Value.ToString();
                if (title.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtFiltrar_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltrar_TextChanged_1(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }
    }
}
