﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_USUARIOS
{
    public partial class EdicionUsuarios : Form
    {

        BindingSource _Roles = new BindingSource();
        BindingSource _Docentes = new BindingSource();

        private void CargarRoles()
        {
            try
            {
                this._Roles.DataSource = CacheManager.SystemCache.TODOSLOSROLES();
                this.cmbUsuario.DataSource = _Roles;
                this.cmbUsuario.DisplayMember = "nombreRol";
                this.cmbUsuario.ValueMember = "idRol";
                this.cmbUsuario.SelectedIndex = 0;
            }
            catch (Exception e)
            {

            }
        }
        private void CargarDocentes()
        {
            try
            {
                this._Docentes.DataSource = CacheManager.SystemCache.DocentesUsuario();
                this.cmbDocente.DataSource = _Docentes;
                this.cmbDocente.DisplayMember = "Nombre";
                this.cmbDocente.ValueMember = "idDocente";
                this.cmbDocente.SelectedIndex = 0;
            }
            catch  (Exception err)
            {

            }
        }
        public void Procesar()
        {
            CLS.Usuario oUsuario = new CLS.Usuario();
            oUsuario.IDUsuario = txtIDUsuario.Text;
            oUsuario.Usuarios = txtNombre.Text;
            oUsuario.Credencial = txtContraseña.Text;
            oUsuario.IDDocente = cmbDocente.SelectedValue.ToString();
            oUsuario.IDRol = cmbUsuario.SelectedValue.ToString();

            // EN CASO DE QUE EL USUARIO NO SEA DOCENTE Y NECESITE TENER UNA CUENTA.
            if (txtIDUsuario.TextLength == 0 &&  cmbUsuario.Text == "Administrador")
            {
                //Estoy insertando un nuevo regitro
                if (oUsuario.GuardarUsuario())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("No pude Insertar");
                }
            }
            else
            {
                if(txtIDUsuario.TextLength != 0 && cmbUsuario.Text == "Administrador")
                {
                    //Estoy actualizando un registro
                    if (oUsuario.ActualizarUsuario())
                    {
                        MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            // EN CASO DE QUE EL USUARIO SEA DOCENTE Y NECESITE TENER UNA CUENTA.
            if (txtIDUsuario.TextLength == 0 && cmbUsuario.Text == "Docente")
            {
                //Estoy insertando un nuevo regitro
                if (oUsuario.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("No pude Insertar");
                }
            }
            else
            {
                if (txtIDUsuario.TextLength != 0 && cmbUsuario.Text == "Docente")
                {
                    //Estoy actualizando un registro
                    if (oUsuario.Actualizar())
                    {
                        MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        public EdicionUsuarios()
        {
            InitializeComponent();
            this.cmbDocente.Enabled = false;
        }

        private void EdicionUsuarios_Load(object sender, EventArgs e)
        {
            CargarRoles();
        }

        private void cmbUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbUsuario.Text == "Docente")
            {
                this.cmbDocente.Enabled = true;
                CargarDocentes();
            }
            else
            {
                this.cmbDocente.Enabled = false;
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void checkMascara_CheckedChanged(object sender, EventArgs e)
        {
            ///Enmascarar y desenmascarar contraseña.
            if(checkMascara.Checked == true)
            {
                if (txtContraseña.PasswordChar == '*')
                {
                    txtContraseña.PasswordChar = '\0';
                }
            }
            else
            {
                this.txtContraseña.PasswordChar = '*';
            }
        }
    }
}
