﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_USUARIOS
{
    public partial class GestionUsuarios : Form
    {
        BindingSource _Usuarios = new BindingSource();

        public void CargarDatos()
        {
            _Usuarios.DataSource = CacheManager.SystemCache.USUARIOS();
            this.dtgvUsuarios.DataSource = _Usuarios;
        }
        public GestionUsuarios()
        {
            InitializeComponent();
        }

        private void GestionUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionUsuarios frm = new EdicionUsuarios();

                frm.cmbUsuario.Text = dtgvUsuarios.CurrentRow.Cells["idRol"].Value.ToString();
                frm.cmbDocente.Text = dtgvUsuarios.CurrentRow.Cells["idDocente"].Value.ToString();
                frm.txtIDUsuario.Text = dtgvUsuarios.CurrentRow.Cells["idUsuarios"].Value.ToString();
                frm.txtNombre.Text = dtgvUsuarios.CurrentRow.Cells["usuario"].Value.ToString();
                frm.txtContraseña.Text = dtgvUsuarios.CurrentRow.Cells["contrasenia"].Value.ToString();
                frm.ShowDialog();
                CargarDatos();
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionUsuarios frm = new EdicionUsuarios();

            frm.ShowDialog();
            CargarDatos();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Usuario oUsuario = new CLS.Usuario();
                oUsuario.IDUsuario = dtgvUsuarios.CurrentRow.Cells["idUsuarios"].Value.ToString();
                if (oUsuario.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
