﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_ENCARGADOS
{
    public partial class GestionEncargados : Form
    {
        BindingSource _Encargados = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                _Encargados.DataSource = CacheManager.SystemCache.TODOSLOSENCARGADOS();
                this.dtgvEncargados.DataSource = _Encargados;
            }
            catch(Exception err) { }

        }
        public GestionEncargados()
        {
            InitializeComponent();
        }

        private void GestionEncargados_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionEncargados frm = new EdicionEncargados();
            frm.ShowDialog();
            CargarDatos();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionEncargados frm = new EdicionEncargados();

                frm.txbIDEncargado.Text = dtgvEncargados.CurrentRow.Cells["idEncargado"].Value.ToString();
                frm.txbNombres.Text = dtgvEncargados.CurrentRow.Cells["nombres_encargado"].Value.ToString();
                frm.txbApellidos.Text = dtgvEncargados.CurrentRow.Cells["apellidos_encargado"].Value.ToString();
                frm.txbTelefono.Text = dtgvEncargados.CurrentRow.Cells["telefono"].Value.ToString();
                frm.txbDUI.Text = dtgvEncargados.CurrentRow.Cells["DUI"].Value.ToString();
                frm.txbTrabajo.Text = dtgvEncargados.CurrentRow.Cells["trabajo"].Value.ToString();

                frm.ShowDialog();
                CargarDatos();
            }
        }
    }
}
