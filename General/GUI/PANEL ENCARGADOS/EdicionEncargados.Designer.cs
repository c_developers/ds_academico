﻿namespace General.GUI.PANEL_ENCARGADOS
{
    partial class EdicionEncargados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.lblDUI = new System.Windows.Forms.Label();
            this.lblTrabajo = new System.Windows.Forms.Label();
            this.Notificador = new System.Windows.Forms.ErrorProvider(this.components);
            this.txbIDEncargado = new System.Windows.Forms.TextBox();
            this.txbNombres = new System.Windows.Forms.TextBox();
            this.txbApellidos = new System.Windows.Forms.TextBox();
            this.txbDUI = new System.Windows.Forms.TextBox();
            this.txbTelefono = new System.Windows.Forms.TextBox();
            this.txbTrabajo = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblIdEncargado = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(201, 81);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(52, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombres:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Location = new System.Drawing.Point(201, 131);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(52, 13);
            this.lblApellidos.TabIndex = 1;
            this.lblApellidos.Text = "Apellidos:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(201, 218);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 13);
            this.lblTelefono.TabIndex = 2;
            this.lblTelefono.Text = "Telefono:";
            // 
            // lblDUI
            // 
            this.lblDUI.AutoSize = true;
            this.lblDUI.Location = new System.Drawing.Point(224, 175);
            this.lblDUI.Name = "lblDUI";
            this.lblDUI.Size = new System.Drawing.Size(29, 13);
            this.lblDUI.TabIndex = 3;
            this.lblDUI.Text = "DUI:";
            // 
            // lblTrabajo
            // 
            this.lblTrabajo.AutoSize = true;
            this.lblTrabajo.Location = new System.Drawing.Point(170, 263);
            this.lblTrabajo.Name = "lblTrabajo";
            this.lblTrabajo.Size = new System.Drawing.Size(85, 13);
            this.lblTrabajo.TabIndex = 4;
            this.lblTrabajo.Text = "Trabajo u Oficio:";
            // 
            // Notificador
            // 
            this.Notificador.ContainerControl = this;
            // 
            // txbIDEncargado
            // 
            this.txbIDEncargado.Enabled = false;
            this.txbIDEncargado.Location = new System.Drawing.Point(271, 42);
            this.txbIDEncargado.Name = "txbIDEncargado";
            this.txbIDEncargado.ReadOnly = true;
            this.txbIDEncargado.Size = new System.Drawing.Size(69, 20);
            this.txbIDEncargado.TabIndex = 5;
            // 
            // txbNombres
            // 
            this.txbNombres.Location = new System.Drawing.Point(271, 81);
            this.txbNombres.Name = "txbNombres";
            this.txbNombres.Size = new System.Drawing.Size(173, 20);
            this.txbNombres.TabIndex = 6;
            // 
            // txbApellidos
            // 
            this.txbApellidos.Location = new System.Drawing.Point(271, 124);
            this.txbApellidos.Name = "txbApellidos";
            this.txbApellidos.Size = new System.Drawing.Size(173, 20);
            this.txbApellidos.TabIndex = 7;
            // 
            // txbDUI
            // 
            this.txbDUI.Location = new System.Drawing.Point(271, 175);
            this.txbDUI.Name = "txbDUI";
            this.txbDUI.Size = new System.Drawing.Size(173, 20);
            this.txbDUI.TabIndex = 8;
            // 
            // txbTelefono
            // 
            this.txbTelefono.Location = new System.Drawing.Point(271, 215);
            this.txbTelefono.Name = "txbTelefono";
            this.txbTelefono.Size = new System.Drawing.Size(173, 20);
            this.txbTelefono.TabIndex = 9;
            // 
            // txbTrabajo
            // 
            this.txbTrabajo.Location = new System.Drawing.Point(271, 256);
            this.txbTrabajo.Name = "txbTrabajo";
            this.txbTrabajo.Size = new System.Drawing.Size(173, 20);
            this.txbTrabajo.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(524, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 49);
            this.button1.TabIndex = 11;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblIdEncargado
            // 
            this.lblIdEncargado.AutoSize = true;
            this.lblIdEncargado.Location = new System.Drawing.Point(192, 45);
            this.lblIdEncargado.Name = "lblIdEncargado";
            this.lblIdEncargado.Size = new System.Drawing.Size(73, 13);
            this.lblIdEncargado.TabIndex = 12;
            this.lblIdEncargado.Text = "ID Encargado";
            // 
            // EdicionEncargados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 386);
            this.Controls.Add(this.lblIdEncargado);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbTrabajo);
            this.Controls.Add(this.txbTelefono);
            this.Controls.Add(this.txbDUI);
            this.Controls.Add(this.txbApellidos);
            this.Controls.Add(this.txbNombres);
            this.Controls.Add(this.txbIDEncargado);
            this.Controls.Add(this.lblTrabajo);
            this.Controls.Add(this.lblDUI);
            this.Controls.Add(this.lblTelefono);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.lblNombre);
            this.Name = "EdicionEncargados";
            this.Text = "EdicionEncargados";
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.Label lblDUI;
        private System.Windows.Forms.Label lblTrabajo;
        private System.Windows.Forms.ErrorProvider Notificador;
        private System.Windows.Forms.Label lblIdEncargado;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox txbTrabajo;
        public System.Windows.Forms.TextBox txbTelefono;
        public System.Windows.Forms.TextBox txbDUI;
        public System.Windows.Forms.TextBox txbApellidos;
        public System.Windows.Forms.TextBox txbNombres;
        public System.Windows.Forms.TextBox txbIDEncargado;
    }
}