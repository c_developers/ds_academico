﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_ENCARGADOS
{
    public partial class EdicionEncargados : Form
    {
  
        public void Procesar()
        {
            CLS.Encargados encargado = new CLS.Encargados();
            encargado.IDEncargado = txbIDEncargado.Text;
            encargado.Nombres = txbNombres.Text;
            encargado.Apellidos = txbApellidos.Text;
            encargado.Telefono = txbTelefono.Text;
            encargado.DUI = txbDUI.Text;
            encargado.Trabajo = txbTrabajo.Text;

            if (txbIDEncargado.TextLength == 0)
            {
                if (encargado.Guardar())
                {
             
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (encargado.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                  }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
        /*
        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtNombre.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtNombre, "Este campo no puede quedar vacío");
            }

            if (txtApellidos.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtApellidos, "Este campo no puede quedar vacío");
            }

            if (txtTelefono.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTelefono, "Este campo no puede quedar vacío");
            }

            if (txtDUI.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtDUI, "Este campo no puede quedar vacío");
            }

            if (txtTrabajo.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtTrabajo, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }*/
        public EdicionEncargados()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            GestionEncargados frm = new GestionEncargados();
            Procesar();

        }
    }
}
