﻿namespace General.GUI.PANEL_ENCARGADOS
{
    partial class GestionEncargados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionEncargados));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.btnAgregar = new System.Windows.Forms.ToolStripButton();
            this.dtgvEncargados = new System.Windows.Forms.DataGridView();
            this.idEncargado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombres_encargado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos_encargado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trabajo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvEncargados)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnEditar,
            this.btnAgregar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnEditar
            // 
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(57, 22);
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(69, 22);
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // dtgvEncargados
            // 
            this.dtgvEncargados.AllowUserToAddRows = false;
            this.dtgvEncargados.AllowUserToDeleteRows = false;
            this.dtgvEncargados.AllowUserToResizeRows = false;
            this.dtgvEncargados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvEncargados.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvEncargados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvEncargados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idEncargado,
            this.nombres_encargado,
            this.apellidos_encargado,
            this.telefono,
            this.DUI,
            this.trabajo});
            this.dtgvEncargados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvEncargados.Location = new System.Drawing.Point(0, 25);
            this.dtgvEncargados.MultiSelect = false;
            this.dtgvEncargados.Name = "dtgvEncargados";
            this.dtgvEncargados.ReadOnly = true;
            this.dtgvEncargados.RowHeadersVisible = false;
            this.dtgvEncargados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvEncargados.Size = new System.Drawing.Size(800, 425);
            this.dtgvEncargados.TabIndex = 1;
            // 
            // idEncargado
            // 
            this.idEncargado.DataPropertyName = "idEncargado";
            this.idEncargado.HeaderText = "idEncargado";
            this.idEncargado.Name = "idEncargado";
            this.idEncargado.ReadOnly = true;
            // 
            // nombres_encargado
            // 
            this.nombres_encargado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nombres_encargado.DataPropertyName = "nombres_encargado";
            this.nombres_encargado.HeaderText = "nombres_encargado";
            this.nombres_encargado.Name = "nombres_encargado";
            this.nombres_encargado.ReadOnly = true;
            // 
            // apellidos_encargado
            // 
            this.apellidos_encargado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.apellidos_encargado.DataPropertyName = "apellidos_encargado";
            this.apellidos_encargado.HeaderText = "apellidos_encargado";
            this.apellidos_encargado.Name = "apellidos_encargado";
            this.apellidos_encargado.ReadOnly = true;
            // 
            // telefono
            // 
            this.telefono.DataPropertyName = "telefono";
            this.telefono.HeaderText = "telefono";
            this.telefono.Name = "telefono";
            this.telefono.ReadOnly = true;
            // 
            // DUI
            // 
            this.DUI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DUI.DataPropertyName = "DUI";
            this.DUI.HeaderText = "DUI";
            this.DUI.Name = "DUI";
            this.DUI.ReadOnly = true;
            // 
            // trabajo
            // 
            this.trabajo.DataPropertyName = "trabajo";
            this.trabajo.HeaderText = "trabajo";
            this.trabajo.Name = "trabajo";
            this.trabajo.ReadOnly = true;
            // 
            // GestionEncargados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dtgvEncargados);
            this.Controls.Add(this.toolStrip1);
            this.Name = "GestionEncargados";
            this.Text = "GestionEncargados";
            this.Load += new System.EventHandler(this.GestionEncargados_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvEncargados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnEditar;
        private System.Windows.Forms.ToolStripButton btnAgregar;
        private System.Windows.Forms.DataGridView dtgvEncargados;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEncargado;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombres_encargado;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos_encargado;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn DUI;
        private System.Windows.Forms.DataGridViewTextBoxColumn trabajo;
    }
}