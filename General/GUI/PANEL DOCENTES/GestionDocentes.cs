﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_DOCENTES
{
    public partial class GestionDocentes : Form
    {
        BindingSource _Docentes = new BindingSource();
        public void CargarDatos()
        {
            _Docentes.DataSource = CacheManager.SystemCache.TODOSLOSDOCENTES();
            this.dtgvDocentes.DataSource = _Docentes;
        }
        public GestionDocentes()
        {
            InitializeComponent();
        }

        private void GestionDocentes_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void dtgvDocentes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (this.dtgvDocentes.Columns[e.ColumnIndex].Name == "laborando")
            {
                try
                {
                    if (e.Value.GetType() != typeof(System.DBNull)) //En caso de haber filas vacias o valores nulos.
                    {
                        if (Convert.ToInt32(e.Value) == 1)
                        {
                            e.CellStyle.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            e.CellStyle.BackColor = Color.Red;
                        }
                    }
                }
                catch (NullReferenceException ex)
                {
                    MessageBox.Show("Error" + ex);
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionDocentes frm = new EdicionDocentes();
                frm.txtIDDocente.Text = dtgvDocentes.CurrentRow.Cells["idDocente"].Value.ToString();
                frm.txtNUSIS.Text = dtgvDocentes.CurrentRow.Cells["NIDocente"].Value.ToString();
                frm.txtNUP.Text = dtgvDocentes.CurrentRow.Cells["NUP"].Value.ToString();
                frm.txtNombres.Text = dtgvDocentes.CurrentRow.Cells["nombre"].Value.ToString();
                frm.txtApellidos.Text = dtgvDocentes.CurrentRow.Cells["apellido"].Value.ToString();
                frm.txtDUI.Text = dtgvDocentes.CurrentRow.Cells["DUI"].Value.ToString();
                frm.txtNIT.Text = dtgvDocentes.CurrentRow.Cells["NIT"].Value.ToString();
                frm.txtTelefono.Text = dtgvDocentes.CurrentRow.Cells["telefono"].Value.ToString();
                frm.txtDireccion.Text = dtgvDocentes.CurrentRow.Cells["direccion"].Value.ToString();
                //frm.rbtLaborando.Text = dtgvDocentes.CurrentRow.Cells["laborando"].Value.ToString();
                //frm.rbtAusente.Text = dtgvDocentes.CurrentRow.Cells["laborando"].Value.ToString();
                frm.cmbTitulos.Text = dtgvDocentes.CurrentRow.Cells["idTitulo"].Value.ToString();
                frm.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Docentes oDocente = new CLS.Docentes();
                oDocente.IdDocente = dtgvDocentes.CurrentRow.Cells["idDocente"].Value.ToString();
                if (oDocente.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionDocentes frm = new EdicionDocentes();
            frm.ShowDialog();
            CargarDatos();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            EdicionDocentes frm = new EdicionDocentes();
            frm.ShowDialog();
            CargarDatos();
        }

        private void btnActualizar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionDocentes frm = new EdicionDocentes();
                frm.txtIDDocente.Text = dtgvDocentes.CurrentRow.Cells["idDocente"].Value.ToString();
                frm.txtNUSIS.Text = dtgvDocentes.CurrentRow.Cells["NIDocente"].Value.ToString();
                frm.txtNUP.Text = dtgvDocentes.CurrentRow.Cells["NUP"].Value.ToString();
                frm.txtNombres.Text = dtgvDocentes.CurrentRow.Cells["nombre"].Value.ToString();
                frm.txtApellidos.Text = dtgvDocentes.CurrentRow.Cells["apellido"].Value.ToString();
                frm.txtDUI.Text = dtgvDocentes.CurrentRow.Cells["DUI"].Value.ToString();
                frm.txtNIT.Text = dtgvDocentes.CurrentRow.Cells["NIT"].Value.ToString();
                frm.txtTelefono.Text = dtgvDocentes.CurrentRow.Cells["telefono"].Value.ToString();
                frm.txtDireccion.Text = dtgvDocentes.CurrentRow.Cells["direccion"].Value.ToString();
                //frm.rbtLaborando.Text = dtgvDocentes.CurrentRow.Cells["laborando"].Value.ToString();
                //frm.rbtAusente.Text = dtgvDocentes.CurrentRow.Cells["laborando"].Value.ToString();
                frm.cmbTitulos.Text = dtgvDocentes.CurrentRow.Cells["idTitulo"].Value.ToString();
                frm.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Docentes oDocente = new CLS.Docentes();
                oDocente.IdDocente = dtgvDocentes.CurrentRow.Cells["idDocente"].Value.ToString();
                if (oDocente.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
