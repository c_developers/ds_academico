﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_DOCENTES
{
    public partial class EdicionDocentes : Form
    {
        BindingSource _EdicionDocente = new BindingSource();
        int estado;
        public void Titulos()
        {
            try
            {
                this._EdicionDocente.DataSource = CacheManager.SystemCache.Titulos();
                this.cmbTitulos.DataSource = _EdicionDocente;
                this.cmbTitulos.DisplayMember = "nombreCarrera";
                this.cmbTitulos.ValueMember = "idTitulo";
                this.cmbTitulos.SelectedIndex = 0;
            }
            catch(Exception err) { }
        }
        public int verificarRadio()
        {
            if (rbtLaborando.Checked)
            {
                estado = 1;
            }
            else
            {
                if (rbtAusente.Checked)
                {
                    estado = 0;
                }  
            }
            return estado;
        }
        public void Procesar()
        {
            CLS.Docentes docentes = new CLS.Docentes();
            docentes.IdDocente = txtIDDocente.Text;
            docentes.NIDocente = txtNUSIS.Text;
            docentes.NUP = txtNUP.Text;
            docentes.Nombre = txtNombres.Text;
            docentes.Apellido = txtApellidos.Text;
            docentes.DUI = txtDUI.Text;
            docentes.NIT = txtNIT.Text;
            docentes.Direccion = txtDireccion.Text;
            docentes.Telefono = txtTelefono.Text;
            docentes.Laborando = verificarRadio();
            docentes.IdTitulo = cmbTitulos.SelectedValue.ToString();
            if (txtIDDocente.TextLength == 0)
            {
                if (docentes.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (docentes.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public EdicionDocentes()
        {
            InitializeComponent();
        }

        private void EdicionDocentes_Load(object sender, EventArgs e)
        {
            GestionDocentes frm = new GestionDocentes();
            Titulos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void btnTitulo_Click(object sender, EventArgs e)
        {
            GUI.PANEL_TITULOS.GestionarTitulos frm = new PANEL_TITULOS.GestionarTitulos();
            frm.ShowDialog();
            Titulos();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lblDUI_Click(object sender, EventArgs e)
        {

        }
    }
}
