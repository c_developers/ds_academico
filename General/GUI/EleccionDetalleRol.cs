﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EleccionDetalleRol : Form
    {
        BindingSource _Roles = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                _Roles.DataSource = CacheManager.SystemCache.TODOSLOSROLES();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Roles.Filter = "Rol LIKE '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Roles.RemoveFilter();
                }
                dtgvDatos.AutoGenerateColumns = false;
                dtgvDatos.DataSource = _Roles;
                lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        public EleccionDetalleRol()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            EdicionUsuarios f = Owner as EdicionUsuarios;

            //Sincronizando interfaz gráfica con registro seleccionado
            f.lblIDRol.Text = dtgvDatos.CurrentRow.Cells["IDRol"].Value.ToString();
            f.txbRol.Text = dtgvDatos.CurrentRow.Cells["Rol"].Value.ToString();
            Close();
        }

        private void dtgvDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EdicionUsuarios f = Owner as EdicionUsuarios;

            //Sincronizando interfaz gráfica con registro seleccionado
            f.lblIDRol.Text = dtgvDatos.CurrentRow.Cells["IDRol"].Value.ToString();
            f.txbRol.Text = dtgvDatos.CurrentRow.Cells["Rol"].Value.ToString();
            Close();
        }

        private void EleccionDetalleRol_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }
    }
}
