﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionUsuarios : Form
    {
      /*  public void Procesar()
        {
            CLS.Usuario oUsuario = new CLS.Usuario();
            oUsuario.IDUsuario = txbIDUsuario.Text;
            oUsuario.usuarios = txbUsuario.Text;
            oUsuario.Credencial = txbPass.Text;
            oUsuario.IDEmpleado = lblIDEmpleado.Text;
            oUsuario.IDRol = lblIDRol.Text;
            oUsuario.Estado = cmbEstado.SelectedItem.ToString();
            if (txbIDUsuario.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (oUsuario.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (oUsuario.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txbUsuario.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbUsuario, "Este campo no puede quedar vacío");
            }

            if (txbPass.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbPass, "Este campo no puede quedar vacío");
            }

            if (txbEmpleado.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbEmpleado, "Este campo no puede quedar vacío");
            }


            if (txbRol.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbRol, "Este campo no puede quedar vacío");
            }

            if (cmbEstado.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cmbEstado, "Este campo no puede quedar vacío");
            }            
            return Verificado;
        }
        public EdicionUsuarios()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnElegirEmpleado_Click(object sender, EventArgs e)
        {
            EleccionDetalleEmpleado f = new EleccionDetalleEmpleado();
            AddOwnedForm(f);
            f.ShowDialog();
        }

        private void btnElegirRol_Click(object sender, EventArgs e)
        {
            EleccionDetalleRol f = new EleccionDetalleRol();
            AddOwnedForm(f);
            f.ShowDialog();
        }

        /*
         * Función para insertar datos a un combo box
         * mostrando en este caso Rol y enviando internamente el ID
        private void CargarRoles()
        {
            cmbEstado.DataSource = CacheManager.SystemCache.TODOSLOSROLES();
            cmbEstado.DisplayMember = "Rol";
            cmbEstado.ValueMember = "IDRol";
        }
        */
        

    }
}
