﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionUsuarios : Form
    {
        BindingSource _Usuarios = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                _Usuarios.DataSource = CacheManager.SystemCache.TODOSLOSUSUARIOS();
                FiltrarLocalmente();
            }
            catch { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Usuarios.Filter = "Empleado LIKE '%" + txtFiltro.Text + "%' OR Usuario like '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Usuarios.RemoveFilter();
                }
                dtgvDatos.AutoGenerateColumns = false;
                dtgvDatos.DataSource = _Usuarios;
                lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }
        public GestionUsuarios()
        {
            InitializeComponent();
        }

        private void GestionUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionUsuarios f = new EdicionUsuarios();
            f.ShowDialog();
            CargarDatos();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionUsuarios f = new EdicionUsuarios();

                //Sincronizando interfaz gráfica con registro seleccionado
                //(Muestra los datos del registro a editar)
                f.txbIDUsuario.Text = dtgvDatos.CurrentRow.Cells["IDUsuario"].Value.ToString();
                f.txbUsuario.Text = dtgvDatos.CurrentRow.Cells["Usuario"].Value.ToString();
                f.txbEmpleado.Text = dtgvDatos.CurrentRow.Cells["Empleado"].Value.ToString();
                f.txbRol.Text = dtgvDatos.CurrentRow.Cells["Rol"].Value.ToString();
                f.cmbEstado.Text = dtgvDatos.CurrentRow.Cells["Estado"].Value.ToString();
                f.lblIDEmpleado.Text = dtgvDatos.CurrentRow.Cells["IDEmpleado"].Value.ToString();
                f.lblIDRol.Text = dtgvDatos.CurrentRow.Cells["IDRol"].Value.ToString();
                f.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea ELIMINAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Usuario oUsuario = new CLS.Usuario();
                oUsuario.IDUsuario = dtgvDatos.CurrentRow.Cells["IDUsuario"].Value.ToString();
                if (oUsuario.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
