﻿namespace General.GUI.PANEL_ESPECIALIDADES
{
    partial class EdicionEspecialidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTituloEspecialidad = new System.Windows.Forms.Label();
            this.lblEspecialidad = new System.Windows.Forms.Label();
            this.txtNombreEspecialidad = new System.Windows.Forms.TextBox();
            this.lblIDEspecialidad = new System.Windows.Forms.Label();
            this.txtIDEspecialidad = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTituloEspecialidad
            // 
            this.lblTituloEspecialidad.AutoSize = true;
            this.lblTituloEspecialidad.Location = new System.Drawing.Point(156, 22);
            this.lblTituloEspecialidad.Name = "lblTituloEspecialidad";
            this.lblTituloEspecialidad.Size = new System.Drawing.Size(180, 13);
            this.lblTituloEspecialidad.TabIndex = 0;
            this.lblTituloEspecialidad.Text = "AGREGAR NUEVA ESPECIALIDAD";
            // 
            // lblEspecialidad
            // 
            this.lblEspecialidad.AutoSize = true;
            this.lblEspecialidad.Location = new System.Drawing.Point(43, 74);
            this.lblEspecialidad.Name = "lblEspecialidad";
            this.lblEspecialidad.Size = new System.Drawing.Size(110, 13);
            this.lblEspecialidad.TabIndex = 1;
            this.lblEspecialidad.Text = "Nombre Especialidad:";
            // 
            // txtNombreEspecialidad
            // 
            this.txtNombreEspecialidad.Location = new System.Drawing.Point(159, 71);
            this.txtNombreEspecialidad.Name = "txtNombreEspecialidad";
            this.txtNombreEspecialidad.Size = new System.Drawing.Size(323, 20);
            this.txtNombreEspecialidad.TabIndex = 2;
            // 
            // lblIDEspecialidad
            // 
            this.lblIDEspecialidad.AutoSize = true;
            this.lblIDEspecialidad.Enabled = false;
            this.lblIDEspecialidad.Location = new System.Drawing.Point(43, 45);
            this.lblIDEspecialidad.Name = "lblIDEspecialidad";
            this.lblIDEspecialidad.Size = new System.Drawing.Size(84, 13);
            this.lblIDEspecialidad.TabIndex = 3;
            this.lblIDEspecialidad.Text = "ID Especialidad:";
            this.lblIDEspecialidad.Visible = false;
            // 
            // txtIDEspecialidad
            // 
            this.txtIDEspecialidad.Enabled = false;
            this.txtIDEspecialidad.Location = new System.Drawing.Point(159, 42);
            this.txtIDEspecialidad.Name = "txtIDEspecialidad";
            this.txtIDEspecialidad.ReadOnly = true;
            this.txtIDEspecialidad.Size = new System.Drawing.Size(46, 20);
            this.txtIDEspecialidad.TabIndex = 4;
            this.txtIDEspecialidad.Visible = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(464, 110);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(102, 39);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Agregar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // EdicionEspecialidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 161);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtIDEspecialidad);
            this.Controls.Add(this.lblIDEspecialidad);
            this.Controls.Add(this.txtNombreEspecialidad);
            this.Controls.Add(this.lblEspecialidad);
            this.Controls.Add(this.lblTituloEspecialidad);
            this.Name = "EdicionEspecialidad";
            this.Text = "EdicionEspecialidad";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTituloEspecialidad;
        private System.Windows.Forms.Label lblEspecialidad;
        private System.Windows.Forms.TextBox txtNombreEspecialidad;
        private System.Windows.Forms.Label lblIDEspecialidad;
        private System.Windows.Forms.TextBox txtIDEspecialidad;
        private System.Windows.Forms.Button btnGuardar;
    }
}