﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_ESPECIALIDADES
{
    public partial class EdicionEspecialidad : Form
    {
        public void Procesar()
        {
            CLS.Especialidades esp = new CLS.Especialidades();

            esp.IdEspecialidad = txtIDEspecialidad.Text;
            esp.NombreEspecialidad = txtNombreEspecialidad.Text;
            if (txtIDEspecialidad.TextLength == 0)
            {
                if (esp.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public EdicionEspecialidad()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }
    }
}
