﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.DEPARTAMENTOS_Y_MUNICIPIOS
{
    public partial class GestionarDepaMuni : Form
    {
        BindingSource Depa = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                Depa.DataSource = CacheManager.SystemCache.DEPA_MUNI();
                dtgvDepaMuni.DataSource = Depa;
            }
            catch (Exception err) { }
        }
        public GestionarDepaMuni()
        {
            InitializeComponent();

        }

        private void GestionarDepaMuni_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EditarDepaMuni frm = new EditarDepaMuni();
                frm.txtIDDepartamento.Text = dtgvDepaMuni.CurrentRow.Cells["idDepartamento"].Value.ToString();
                frm.txtDepartamento.Text = dtgvDepaMuni.CurrentRow.Cells["departamento"].Value.ToString();
                frm.txtIDMunicipio.Text = dtgvDepaMuni.CurrentRow.Cells["idMunicipio"].Value.ToString();
                frm.txtMunicipio.Text = dtgvDepaMuni.CurrentRow.Cells["municipio"].Value.ToString();
                frm.ShowDialog();
                CargarDatos();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Departamentos depa = new CLS.Departamentos();
                depa.IDDepartamento = dtgvDepaMuni.CurrentRow.Cells["idDepartamento"].Value.ToString();
                if (depa.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser eliminado exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EditarDepaMuni frm = new EditarDepaMuni();
            frm.ShowDialog();
            CargarDatos();
        }
    }
}
