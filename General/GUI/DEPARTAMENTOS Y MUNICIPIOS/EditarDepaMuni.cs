﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.DEPARTAMENTOS_Y_MUNICIPIOS
{
    public partial class EditarDepaMuni : Form
    {
        BindingSource Departamentos = new BindingSource();

        public void CargarDepartamentos()
        {
            try
            {
                this.Departamentos.DataSource = CacheManager.SystemCache.DEPARTAMENTOS();
                this.cmbDepartamentos.DataSource = Departamentos;
                this.cmbDepartamentos.DisplayMember = "departamento";
                this.cmbDepartamentos.ValueMember = "idDepartamento";
                this.cmbDepartamentos.SelectedIndex = 0;
            }catch(Exception err) { }
        }
        public void Procesar()
        {
            General.CLS.Departamentos depa = new CLS.Departamentos();
            depa.IDDepartamento = txtIDDepartamento.Text;
            depa.Departamento = txtDepartamento.Text;

            if (txtIDDepartamento.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (depa.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (depa.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
        public void ProcesarMunicipio()
        {
            General.CLS.Municipios muni = new CLS.Municipios();
            muni.IdMunicipio = txtIDMunicipio.Text;
            muni.Municipio = txtMunicipio.Text;
            muni.IdDepartamento = cmbDepartamentos.SelectedValue.ToString();

            if (txtIDMunicipio.TextLength == 0)
            {
                //Estoy insertando un nuevo regitro
                if (muni.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (muni.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        public EditarDepaMuni()
        {
            InitializeComponent();
        }

        private void btnDepartamento_Click(object sender, EventArgs e)
        {
            Procesar();
            CargarDepartamentos();
        }

        private void EditarDepaMuni_Load(object sender, EventArgs e)
        {
            CargarDepartamentos();
        }

        private void btnAgregarMunicipio_Click(object sender, EventArgs e)
        {
            ProcesarMunicipio();
        }
    }
}
