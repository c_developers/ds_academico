﻿namespace General.GUI.DEPARTAMENTOS_Y_MUNICIPIOS
{
    partial class EditarDepaMuni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIDDepartamento = new System.Windows.Forms.TextBox();
            this.btnDepartamento = new System.Windows.Forms.Button();
            this.txtDepartamento = new System.Windows.Forms.TextBox();
            this.lblDepartamento = new System.Windows.Forms.Label();
            this.txtIDMunicipio = new System.Windows.Forms.TextBox();
            this.lblIDMunicipio = new System.Windows.Forms.Label();
            this.lblDepa = new System.Windows.Forms.Label();
            this.btnAgregarMunicipio = new System.Windows.Forms.Button();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.cmbDepartamentos = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.txtIDDepartamento);
            this.splitContainer1.Panel1.Controls.Add(this.btnDepartamento);
            this.splitContainer1.Panel1.Controls.Add(this.txtDepartamento);
            this.splitContainer1.Panel1.Controls.Add(this.lblDepartamento);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtIDMunicipio);
            this.splitContainer1.Panel2.Controls.Add(this.lblIDMunicipio);
            this.splitContainer1.Panel2.Controls.Add(this.lblDepa);
            this.splitContainer1.Panel2.Controls.Add(this.btnAgregarMunicipio);
            this.splitContainer1.Panel2.Controls.Add(this.txtMunicipio);
            this.splitContainer1.Panel2.Controls.Add(this.cmbDepartamentos);
            this.splitContainer1.Size = new System.Drawing.Size(657, 295);
            this.splitContainer1.SplitterDistance = 330;
            this.splitContainer1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID Departamento";
            // 
            // txtIDDepartamento
            // 
            this.txtIDDepartamento.Enabled = false;
            this.txtIDDepartamento.Location = new System.Drawing.Point(39, 99);
            this.txtIDDepartamento.Name = "txtIDDepartamento";
            this.txtIDDepartamento.ReadOnly = true;
            this.txtIDDepartamento.Size = new System.Drawing.Size(71, 20);
            this.txtIDDepartamento.TabIndex = 3;
            // 
            // btnDepartamento
            // 
            this.btnDepartamento.Location = new System.Drawing.Point(185, 238);
            this.btnDepartamento.Name = "btnDepartamento";
            this.btnDepartamento.Size = new System.Drawing.Size(133, 40);
            this.btnDepartamento.TabIndex = 2;
            this.btnDepartamento.Text = "Insertar Departamento";
            this.btnDepartamento.UseVisualStyleBackColor = true;
            this.btnDepartamento.Click += new System.EventHandler(this.btnDepartamento_Click);
            // 
            // txtDepartamento
            // 
            this.txtDepartamento.Location = new System.Drawing.Point(39, 153);
            this.txtDepartamento.Name = "txtDepartamento";
            this.txtDepartamento.Size = new System.Drawing.Size(220, 20);
            this.txtDepartamento.TabIndex = 1;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.AutoSize = true;
            this.lblDepartamento.Location = new System.Drawing.Point(36, 131);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(74, 13);
            this.lblDepartamento.TabIndex = 0;
            this.lblDepartamento.Text = "Departamento";
            // 
            // txtIDMunicipio
            // 
            this.txtIDMunicipio.Enabled = false;
            this.txtIDMunicipio.Location = new System.Drawing.Point(56, 88);
            this.txtIDMunicipio.Name = "txtIDMunicipio";
            this.txtIDMunicipio.ReadOnly = true;
            this.txtIDMunicipio.Size = new System.Drawing.Size(71, 20);
            this.txtIDMunicipio.TabIndex = 5;
            // 
            // lblIDMunicipio
            // 
            this.lblIDMunicipio.AutoSize = true;
            this.lblIDMunicipio.Location = new System.Drawing.Point(53, 69);
            this.lblIDMunicipio.Name = "lblIDMunicipio";
            this.lblIDMunicipio.Size = new System.Drawing.Size(66, 13);
            this.lblIDMunicipio.TabIndex = 5;
            this.lblIDMunicipio.Text = "ID Municipio";
            // 
            // lblDepa
            // 
            this.lblDepa.AutoSize = true;
            this.lblDepa.Location = new System.Drawing.Point(53, 40);
            this.lblDepa.Name = "lblDepa";
            this.lblDepa.Size = new System.Drawing.Size(143, 13);
            this.lblDepa.TabIndex = 3;
            this.lblDepa.Text = "Seleccione un departamento";
            // 
            // btnAgregarMunicipio
            // 
            this.btnAgregarMunicipio.Location = new System.Drawing.Point(203, 238);
            this.btnAgregarMunicipio.Name = "btnAgregarMunicipio";
            this.btnAgregarMunicipio.Size = new System.Drawing.Size(108, 40);
            this.btnAgregarMunicipio.TabIndex = 2;
            this.btnAgregarMunicipio.Text = "Agregar Municipio";
            this.btnAgregarMunicipio.UseVisualStyleBackColor = true;
            this.btnAgregarMunicipio.Click += new System.EventHandler(this.btnAgregarMunicipio_Click);
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.Location = new System.Drawing.Point(56, 153);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(201, 20);
            this.txtMunicipio.TabIndex = 1;
            // 
            // cmbDepartamentos
            // 
            this.cmbDepartamentos.FormattingEnabled = true;
            this.cmbDepartamentos.Location = new System.Drawing.Point(56, 114);
            this.cmbDepartamentos.Name = "cmbDepartamentos";
            this.cmbDepartamentos.Size = new System.Drawing.Size(201, 21);
            this.cmbDepartamentos.TabIndex = 0;
            // 
            // EditarDepaMuni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 295);
            this.Controls.Add(this.splitContainer1);
            this.Name = "EditarDepaMuni";
            this.Text = "EditarDepaMuni";
            this.Load += new System.EventHandler(this.EditarDepaMuni_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnDepartamento;
        private System.Windows.Forms.Button btnAgregarMunicipio;
        public System.Windows.Forms.TextBox txtDepartamento;
        public System.Windows.Forms.Label lblDepartamento;
        public System.Windows.Forms.Label lblDepa;
        public System.Windows.Forms.TextBox txtMunicipio;
        public System.Windows.Forms.ComboBox cmbDepartamentos;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtIDDepartamento;
        public System.Windows.Forms.TextBox txtIDMunicipio;
        public System.Windows.Forms.Label lblIDMunicipio;
    }
}