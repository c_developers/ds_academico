﻿namespace General.GUI.PANEL_CONSULTAS
{
    partial class Alumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBusqueda = new System.Windows.Forms.Label();
            this.lblNIE = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNIE = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.lblFoto = new System.Windows.Forms.Label();
            this.lblEstado = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.dtgvAlumnos = new System.Windows.Forms.DataGridView();
            this.NIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PApellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SApellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zonaUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechNacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anioInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anioGraducion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBusqueda
            // 
            this.lblBusqueda.AutoSize = true;
            this.lblBusqueda.Location = new System.Drawing.Point(12, 21);
            this.lblBusqueda.Name = "lblBusqueda";
            this.lblBusqueda.Size = new System.Drawing.Size(190, 13);
            this.lblBusqueda.TabIndex = 0;
            this.lblBusqueda.Text = "Ingrese el nombre o NIE del estudiante";
            // 
            // lblNIE
            // 
            this.lblNIE.AutoSize = true;
            this.lblNIE.Location = new System.Drawing.Point(12, 64);
            this.lblNIE.Name = "lblNIE";
            this.lblNIE.Size = new System.Drawing.Size(28, 13);
            this.lblNIE.TabIndex = 1;
            this.lblNIE.Text = "NIE:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(12, 95);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtNIE
            // 
            this.txtNIE.Location = new System.Drawing.Point(70, 61);
            this.txtNIE.Name = "txtNIE";
            this.txtNIE.Size = new System.Drawing.Size(132, 20);
            this.txtNIE.TabIndex = 3;
            this.txtNIE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNIE_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(70, 92);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(265, 20);
            this.txtNombre.TabIndex = 4;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(215, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "*Debe digitar primer nombre y primer apellido";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(231, 46);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(92, 35);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // lblFoto
            // 
            this.lblFoto.Location = new System.Drawing.Point(437, 21);
            this.lblFoto.Name = "lblFoto";
            this.lblFoto.Size = new System.Drawing.Size(126, 131);
            this.lblFoto.TabIndex = 7;
            this.lblFoto.Text = "Foto";
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(12, 164);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(43, 13);
            this.lblEstado.TabIndex = 8;
            this.lblEstado.Text = "Estado:";
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(70, 164);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(99, 20);
            this.txtEstado.TabIndex = 9;
            // 
            // dtgvAlumnos
            // 
            this.dtgvAlumnos.AllowUserToAddRows = false;
            this.dtgvAlumnos.AllowUserToDeleteRows = false;
            this.dtgvAlumnos.AllowUserToResizeRows = false;
            this.dtgvAlumnos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvAlumnos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NIE,
            this.PNombre,
            this.SNombre,
            this.PApellido,
            this.SApellido,
            this.zonaUR,
            this.fechNacimiento,
            this.anioInicio,
            this.anioGraducion});
            this.dtgvAlumnos.Location = new System.Drawing.Point(0, 190);
            this.dtgvAlumnos.MultiSelect = false;
            this.dtgvAlumnos.Name = "dtgvAlumnos";
            this.dtgvAlumnos.ReadOnly = true;
            this.dtgvAlumnos.RowHeadersVisible = false;
            this.dtgvAlumnos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvAlumnos.Size = new System.Drawing.Size(596, 150);
            this.dtgvAlumnos.TabIndex = 10;
            this.dtgvAlumnos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgvAlumnos_CellClick);
            // 
            // NIE
            // 
            this.NIE.DataPropertyName = "NIE";
            this.NIE.HeaderText = "NIE";
            this.NIE.Name = "NIE";
            this.NIE.ReadOnly = true;
            // 
            // PNombre
            // 
            this.PNombre.DataPropertyName = "PNombre";
            this.PNombre.HeaderText = "PNombre";
            this.PNombre.Name = "PNombre";
            this.PNombre.ReadOnly = true;
            // 
            // SNombre
            // 
            this.SNombre.DataPropertyName = "SNombre";
            this.SNombre.HeaderText = "SNombre";
            this.SNombre.Name = "SNombre";
            this.SNombre.ReadOnly = true;
            // 
            // PApellido
            // 
            this.PApellido.DataPropertyName = "PApellido";
            this.PApellido.HeaderText = "PApellido";
            this.PApellido.Name = "PApellido";
            this.PApellido.ReadOnly = true;
            // 
            // SApellido
            // 
            this.SApellido.DataPropertyName = "SApellido";
            this.SApellido.HeaderText = "SApellido";
            this.SApellido.Name = "SApellido";
            this.SApellido.ReadOnly = true;
            // 
            // zonaUR
            // 
            this.zonaUR.DataPropertyName = "zonaUR";
            this.zonaUR.HeaderText = "Zona";
            this.zonaUR.Name = "zonaUR";
            this.zonaUR.ReadOnly = true;
            // 
            // fechNacimiento
            // 
            this.fechNacimiento.DataPropertyName = "fechNacimiento";
            this.fechNacimiento.HeaderText = "FechaNacimiento";
            this.fechNacimiento.Name = "fechNacimiento";
            this.fechNacimiento.ReadOnly = true;
            // 
            // anioInicio
            // 
            this.anioInicio.DataPropertyName = "anioInicio";
            this.anioInicio.HeaderText = "anioInicio";
            this.anioInicio.Name = "anioInicio";
            this.anioInicio.ReadOnly = true;
            // 
            // anioGraducion
            // 
            this.anioGraducion.DataPropertyName = "anioGraducion";
            this.anioGraducion.HeaderText = "anioGraduacion";
            this.anioGraducion.Name = "anioGraducion";
            this.anioGraducion.ReadOnly = true;
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Location = new System.Drawing.Point(12, 352);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(43, 13);
            this.lblDireccion.TabIndex = 11;
            this.lblDireccion.Text = "Estado:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(61, 352);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(374, 20);
            this.txtDireccion.TabIndex = 12;
            // 
            // Alumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 450);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.lblDireccion);
            this.Controls.Add(this.dtgvAlumnos);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.lblEstado);
            this.Controls.Add(this.lblFoto);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtNIE);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblNIE);
            this.Controls.Add(this.lblBusqueda);
            this.Name = "Alumnos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alumnos";
            ((System.ComponentModel.ISupportInitialize)(this.dtgvAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBusqueda;
        private System.Windows.Forms.Label lblNIE;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNIE;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Label lblFoto;
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.DataGridView dtgvAlumnos;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn PApellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn SApellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn zonaUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechNacimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn anioInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn anioGraducion;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.TextBox txtDireccion;
    }
}