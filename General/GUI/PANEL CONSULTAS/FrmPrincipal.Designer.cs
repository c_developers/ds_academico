﻿namespace General.GUI.PANEL_CONSULTAS
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDocentes = new System.Windows.Forms.Button();
            this.btnAlumnos = new System.Windows.Forms.Button();
            this.lblInformacion = new System.Windows.Forms.Label();
            this.lblInConsultar = new System.Windows.Forms.Label();
            this.lblIcono = new System.Windows.Forms.Label();
            this.lblAtras = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.btnDocentes);
            this.panel1.Controls.Add(this.btnAlumnos);
            this.panel1.Controls.Add(this.lblInformacion);
            this.panel1.Controls.Add(this.lblInConsultar);
            this.panel1.Controls.Add(this.lblIcono);
            this.panel1.Controls.Add(this.lblAtras);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 450);
            this.panel1.TabIndex = 0;
            // 
            // btnDocentes
            // 
            this.btnDocentes.Image = global::General.Properties.Resources.profesor;
            this.btnDocentes.Location = new System.Drawing.Point(140, 103);
            this.btnDocentes.Name = "btnDocentes";
            this.btnDocentes.Size = new System.Drawing.Size(125, 110);
            this.btnDocentes.TabIndex = 5;
            this.btnDocentes.Text = "Docentes";
            this.btnDocentes.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDocentes.UseVisualStyleBackColor = true;
            this.btnDocentes.Click += new System.EventHandler(this.btnDocentes_Click);
            // 
            // btnAlumnos
            // 
            this.btnAlumnos.Image = global::General.Properties.Resources.study__1_;
            this.btnAlumnos.Location = new System.Drawing.Point(0, 103);
            this.btnAlumnos.Name = "btnAlumnos";
            this.btnAlumnos.Size = new System.Drawing.Size(125, 110);
            this.btnAlumnos.TabIndex = 4;
            this.btnAlumnos.Text = "Alumnos";
            this.btnAlumnos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAlumnos.UseVisualStyleBackColor = true;
            this.btnAlumnos.Click += new System.EventHandler(this.btnAlumnos_Click);
            // 
            // lblInformacion
            // 
            this.lblInformacion.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.Location = new System.Drawing.Point(122, 32);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(143, 23);
            this.lblInformacion.TabIndex = 3;
            this.lblInformacion.Text = "Informacion";
            // 
            // lblInConsultar
            // 
            this.lblInConsultar.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInConsultar.Location = new System.Drawing.Point(135, 9);
            this.lblInConsultar.Name = "lblInConsultar";
            this.lblInConsultar.Size = new System.Drawing.Size(113, 23);
            this.lblInConsultar.TabIndex = 2;
            this.lblInConsultar.Text = "Consultar";
            // 
            // lblIcono
            // 
            this.lblIcono.Image = global::General.Properties.Resources.website;
            this.lblIcono.Location = new System.Drawing.Point(40, 0);
            this.lblIcono.Name = "lblIcono";
            this.lblIcono.Size = new System.Drawing.Size(100, 70);
            this.lblIcono.TabIndex = 1;
            // 
            // lblAtras
            // 
            this.lblAtras.Image = global::General.Properties.Resources.left_arrow__1_;
            this.lblAtras.Location = new System.Drawing.Point(3, 9);
            this.lblAtras.Name = "lblAtras";
            this.lblAtras.Size = new System.Drawing.Size(51, 45);
            this.lblAtras.TabIndex = 0;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "FrmPrincipal";
            this.Text = "FrmPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblIcono;
        private System.Windows.Forms.Label lblAtras;
        private System.Windows.Forms.Button btnAlumnos;
        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.Label lblInConsultar;
        private System.Windows.Forms.Button btnDocentes;
    }
}