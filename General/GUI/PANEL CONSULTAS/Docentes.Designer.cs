﻿namespace General.GUI.PANEL_CONSULTAS
{
    partial class Docentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNIP = new System.Windows.Forms.Label();
            this.txtNIP = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblEtiqueta = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnTodo = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dtgvDocentes = new System.Windows.Forms.DataGridView();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.idDocente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NIDocente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laborando = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreCarrera = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDocentes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNIP
            // 
            this.lblNIP.AutoSize = true;
            this.lblNIP.Location = new System.Drawing.Point(48, 40);
            this.lblNIP.Name = "lblNIP";
            this.lblNIP.Size = new System.Drawing.Size(31, 13);
            this.lblNIP.TabIndex = 0;
            this.lblNIP.Text = "NIP: ";
            // 
            // txtNIP
            // 
            this.txtNIP.Location = new System.Drawing.Point(85, 40);
            this.txtNIP.Name = "txtNIP";
            this.txtNIP.Size = new System.Drawing.Size(165, 20);
            this.txtNIP.TabIndex = 1;
            this.txtNIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNIP_KeyPress);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(85, 85);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(264, 20);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(29, 85);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(50, 13);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre: ";
            // 
            // lblEtiqueta
            // 
            this.lblEtiqueta.AutoSize = true;
            this.lblEtiqueta.Location = new System.Drawing.Point(82, 121);
            this.lblEtiqueta.Name = "lblEtiqueta";
            this.lblEtiqueta.Size = new System.Drawing.Size(224, 13);
            this.lblEtiqueta.TabIndex = 4;
            this.lblEtiqueta.Text = "*Debe ingresar primer nombre y primer apellido";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(457, 24);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(104, 44);
            this.btnConsultar.TabIndex = 5;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnTodo
            // 
            this.btnTodo.Location = new System.Drawing.Point(457, 74);
            this.btnTodo.Name = "btnTodo";
            this.btnTodo.Size = new System.Drawing.Size(104, 44);
            this.btnTodo.TabIndex = 6;
            this.btnTodo.Text = "Mostrar Todo";
            this.btnTodo.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(457, 124);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(104, 44);
            this.button3.TabIndex = 7;
            this.button3.Text = "Horario";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dtgvDocentes
            // 
            this.dtgvDocentes.AllowUserToAddRows = false;
            this.dtgvDocentes.AllowUserToDeleteRows = false;
            this.dtgvDocentes.AllowUserToResizeRows = false;
            this.dtgvDocentes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvDocentes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvDocentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDocentes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDocente,
            this.NIDocente,
            this.NUP,
            this.nombre,
            this.apellido,
            this.DUI,
            this.NIT,
            this.telefono,
            this.direccion,
            this.laborando,
            this.nombreCarrera});
            this.dtgvDocentes.Location = new System.Drawing.Point(-3, 215);
            this.dtgvDocentes.MultiSelect = false;
            this.dtgvDocentes.Name = "dtgvDocentes";
            this.dtgvDocentes.ReadOnly = true;
            this.dtgvDocentes.RowHeadersVisible = false;
            this.dtgvDocentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvDocentes.Size = new System.Drawing.Size(592, 215);
            this.dtgvDocentes.TabIndex = 8;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(85, 176);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(168, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "Buscar aunque no este activo";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // idDocente
            // 
            this.idDocente.DataPropertyName = "idDocente";
            this.idDocente.HeaderText = "No.";
            this.idDocente.Name = "idDocente";
            this.idDocente.ReadOnly = true;
            // 
            // NIDocente
            // 
            this.NIDocente.DataPropertyName = "NIDocente";
            this.NIDocente.HeaderText = "NIP";
            this.NIDocente.Name = "NIDocente";
            this.NIDocente.ReadOnly = true;
            // 
            // NUP
            // 
            this.NUP.DataPropertyName = "NUP";
            this.NUP.HeaderText = "NUP";
            this.NUP.Name = "NUP";
            this.NUP.ReadOnly = true;
            // 
            // nombre
            // 
            this.nombre.DataPropertyName = "nombre";
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            // 
            // apellido
            // 
            this.apellido.DataPropertyName = "apellido";
            this.apellido.HeaderText = "Apellido";
            this.apellido.Name = "apellido";
            this.apellido.ReadOnly = true;
            // 
            // DUI
            // 
            this.DUI.DataPropertyName = "DUI";
            this.DUI.HeaderText = "DUI";
            this.DUI.Name = "DUI";
            this.DUI.ReadOnly = true;
            // 
            // NIT
            // 
            this.NIT.DataPropertyName = "NIT";
            this.NIT.HeaderText = "NIT";
            this.NIT.Name = "NIT";
            this.NIT.ReadOnly = true;
            // 
            // telefono
            // 
            this.telefono.DataPropertyName = "telefono";
            this.telefono.HeaderText = "Telefono";
            this.telefono.Name = "telefono";
            this.telefono.ReadOnly = true;
            // 
            // direccion
            // 
            this.direccion.DataPropertyName = "direccion";
            this.direccion.HeaderText = "Direccion";
            this.direccion.Name = "direccion";
            this.direccion.ReadOnly = true;
            // 
            // laborando
            // 
            this.laborando.DataPropertyName = "laborando";
            this.laborando.HeaderText = "Laborando";
            this.laborando.Name = "laborando";
            this.laborando.ReadOnly = true;
            // 
            // nombreCarrera
            // 
            this.nombreCarrera.DataPropertyName = "nombreCarrera";
            this.nombreCarrera.HeaderText = "Titulo";
            this.nombreCarrera.Name = "nombreCarrera";
            this.nombreCarrera.ReadOnly = true;
            // 
            // Docentes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 426);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.dtgvDocentes);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnTodo);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.lblEtiqueta);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.txtNIP);
            this.Controls.Add(this.lblNIP);
            this.Name = "Docentes";
            this.Text = "Docentes";
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDocentes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNIP;
        private System.Windows.Forms.TextBox txtNIP;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblEtiqueta;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnTodo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dtgvDocentes;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDocente;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIDocente;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUP;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn DUI;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn laborando;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreCarrera;
    }
}