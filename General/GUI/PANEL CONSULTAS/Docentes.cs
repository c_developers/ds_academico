﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_CONSULTAS
{
    public partial class Docentes : Form
    {
        BindingSource data = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                // alumnos.DataSource = CacheManager.SystemCache.Alumnos(txtNIE.Text);

                //this.dtgvDocentes.AutoGenerateColumns = false;
                this.dtgvDocentes.DataSource = data;
            }
            catch
            {

            }
        }
        public Docentes()
        {
            InitializeComponent();
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (CacheManager.SystemCache.Enter(e))
            {
                //CacheManager.SystemCache.Docentes(txtNIP.Text);
                CargarDatos();
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CacheManager.SystemCache.Docentes();
            CargarDatos();
        }
    }
}
