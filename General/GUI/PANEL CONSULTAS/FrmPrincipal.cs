﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_CONSULTAS
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void btnAlumnos_Click(object sender, EventArgs e)
        {
            Alumnos frm = new Alumnos();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }

        private void btnDocentes_Click(object sender, EventArgs e)
        {
            Docentes frm = new Docentes();
            frm.MdiParent = this.ParentForm;
            frm.Show();
        }
    }
}
