﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_CONSULTAS
{
    public partial class Alumnos : Form
    {
        BindingSource alumnos = new BindingSource();

        public void CargarDatos()
        {
            try
            {
                // alumnos.DataSource = CacheManager.SystemCache.Alumnos(txtNIE.Text);
                this.dtgvAlumnos.AutoGenerateColumns = false;
                this.dtgvAlumnos.DataSource = alumnos;
            }
            catch
            {

            }
        }
        public Alumnos()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            String NIE = txtNIE.Text;
            String Nombre = txtNombre.Text;
            if(NIE == "" && Nombre == "")
            {
                MessageBox.Show("Debe rellenar al menos un campo");
            }
            else
            {
                if(NIE != "")
                {
                    alumnos.DataSource = CacheManager.SystemCache.Alumnos(txtNIE.Text);
                    CargarDatos();
                    
                }
                else
                {
                    alumnos.DataSource = CacheManager.SystemCache.AlumnosNombre(txtNombre.Text);
                    CargarDatos();
                }
            }
        }

        private void txtNIE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (CacheManager.SystemCache.Enter(e))
            {
                alumnos.DataSource = CacheManager.SystemCache.Alumnos(txtNIE.Text);
                CargarDatos();
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (CacheManager.SystemCache.Enter(e))
            {
                alumnos.DataSource = CacheManager.SystemCache.AlumnosNombre(txtNombre.Text);
                CargarDatos();
            }
        }

        private void dtgvAlumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //this.txtDireccion.Text = this.dtgvAlumnos.CurrentRow.Cells[9].Value.ToString();
        }
    }
}
