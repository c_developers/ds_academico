﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionPermisos : Form
    {
        BindingSource _Permisos = new BindingSource();
        String Valor;

        private void CargarRoles()
        {
            try
            {
                this._Permisos.DataSource = CacheManager.SystemCache.TODOSLOSROLES();
                this.cmbRoles.DataSource = _Permisos;
                this.cmbRoles.DisplayMember = "nombreRol";
                this.cmbRoles.ValueMember = "idRol";
                this.cmbRoles.SelectedIndex = 0;
            }
            catch {
            }
        }
        private void CargarPermisos()
        {
            try
            {
                this.dtgvPermisos.DataSource = CacheManager.SystemCache.PERMISOSDEUNROL(cmbRoles.SelectedValue.ToString());
            }
            catch { }
        }
        public GestionPermisos()
        {
            InitializeComponent();
            this.CargarRoles();
        }

        private void GestionPermisos_Load(object sender, EventArgs e)
        {
            this.CargarRoles();
        }

        private void cmbRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarPermisos();
            this.dtgvPermisos.Refresh();
        }

        private void dtgvPermisos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Valor = dtgvPermisos.CurrentRow.Cells["Asignado"].Value.ToString();
            //MessageBox.Show(Valor);
        }
    }
}
