﻿namespace General.GUI.PANEL_ALUMNOS
{
    partial class GestionAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionAlumnos));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.txtFiltro = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolEliminar = new System.Windows.Forms.ToolStripButton();
            this.toolEditar = new System.Windows.Forms.ToolStripButton();
            this.toolAgregar = new System.Windows.Forms.ToolStripButton();
            this.dtgvAlumnos = new System.Windows.Forms.DataGridView();
            this.NIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Papellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sapellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDepartamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idMunicipio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zonaUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechNacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alergias_enfermedades = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatus_descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anioInicio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anioGraducion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtFiltro,
            this.toolStripLabel1,
            this.toolEliminar,
            this.toolEditar,
            this.toolAgregar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(963, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // txtFiltro
            // 
            this.txtFiltro.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.txtFiltro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(300, 25);
            this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(37, 22);
            this.toolStripLabel1.Text = "Filtrar";
            // 
            // toolEliminar
            // 
            this.toolEliminar.Image = ((System.Drawing.Image)(resources.GetObject("toolEliminar.Image")));
            this.toolEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolEliminar.Name = "toolEliminar";
            this.toolEliminar.Size = new System.Drawing.Size(70, 22);
            this.toolEliminar.Text = "Eliminar";
            this.toolEliminar.Click += new System.EventHandler(this.toolEliminar_Click);
            // 
            // toolEditar
            // 
            this.toolEditar.Image = ((System.Drawing.Image)(resources.GetObject("toolEditar.Image")));
            this.toolEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolEditar.Name = "toolEditar";
            this.toolEditar.Size = new System.Drawing.Size(57, 22);
            this.toolEditar.Text = "Editar";
            this.toolEditar.Click += new System.EventHandler(this.toolEditar_Click);
            // 
            // toolAgregar
            // 
            this.toolAgregar.Image = ((System.Drawing.Image)(resources.GetObject("toolAgregar.Image")));
            this.toolAgregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolAgregar.Name = "toolAgregar";
            this.toolAgregar.Size = new System.Drawing.Size(69, 22);
            this.toolAgregar.Text = "Agregar";
            this.toolAgregar.Click += new System.EventHandler(this.toolAgregar_Click);
            // 
            // dtgvAlumnos
            // 
            this.dtgvAlumnos.AllowUserToAddRows = false;
            this.dtgvAlumnos.AllowUserToDeleteRows = false;
            this.dtgvAlumnos.AllowUserToResizeRows = false;
            this.dtgvAlumnos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgvAlumnos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.dtgvAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvAlumnos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NIE,
            this.PNombre,
            this.SNombre,
            this.Papellido,
            this.Sapellido,
            this.idDepartamento,
            this.idMunicipio,
            this.zonaUR,
            this.direccion,
            this.calle,
            this.fechNacimiento,
            this.alergias_enfermedades,
            this.estatus,
            this.estatus_descripcion,
            this.anioInicio,
            this.anioGraducion});
            this.dtgvAlumnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvAlumnos.Location = new System.Drawing.Point(0, 25);
            this.dtgvAlumnos.MultiSelect = false;
            this.dtgvAlumnos.Name = "dtgvAlumnos";
            this.dtgvAlumnos.ReadOnly = true;
            this.dtgvAlumnos.RowHeadersVisible = false;
            this.dtgvAlumnos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvAlumnos.Size = new System.Drawing.Size(963, 236);
            this.dtgvAlumnos.TabIndex = 2;
            // 
            // NIE
            // 
            this.NIE.DataPropertyName = "NIE";
            this.NIE.HeaderText = "NIE";
            this.NIE.Name = "NIE";
            this.NIE.ReadOnly = true;
            // 
            // PNombre
            // 
            this.PNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PNombre.DataPropertyName = "PNombre";
            this.PNombre.HeaderText = "Primer Nombre";
            this.PNombre.Name = "PNombre";
            this.PNombre.ReadOnly = true;
            // 
            // SNombre
            // 
            this.SNombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SNombre.DataPropertyName = "SNombre";
            this.SNombre.HeaderText = "Segundo Nombre";
            this.SNombre.Name = "SNombre";
            this.SNombre.ReadOnly = true;
            // 
            // Papellido
            // 
            this.Papellido.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Papellido.DataPropertyName = "Papellido";
            this.Papellido.HeaderText = "Primer Apellido";
            this.Papellido.Name = "Papellido";
            this.Papellido.ReadOnly = true;
            // 
            // Sapellido
            // 
            this.Sapellido.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Sapellido.DataPropertyName = "Sapellido";
            this.Sapellido.HeaderText = "Segundo Apellido";
            this.Sapellido.Name = "Sapellido";
            this.Sapellido.ReadOnly = true;
            // 
            // idDepartamento
            // 
            this.idDepartamento.DataPropertyName = "idDepartamento";
            this.idDepartamento.HeaderText = "idDepartamento";
            this.idDepartamento.Name = "idDepartamento";
            this.idDepartamento.ReadOnly = true;
            this.idDepartamento.Visible = false;
            // 
            // idMunicipio
            // 
            this.idMunicipio.DataPropertyName = "idMunicipio";
            this.idMunicipio.HeaderText = "idMunicipio";
            this.idMunicipio.Name = "idMunicipio";
            this.idMunicipio.ReadOnly = true;
            this.idMunicipio.Visible = false;
            // 
            // zonaUR
            // 
            this.zonaUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.zonaUR.DataPropertyName = "zonaUR";
            this.zonaUR.HeaderText = "Zona";
            this.zonaUR.Name = "zonaUR";
            this.zonaUR.ReadOnly = true;
            // 
            // direccion
            // 
            this.direccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.direccion.DataPropertyName = "direccion";
            this.direccion.HeaderText = "Direccion";
            this.direccion.Name = "direccion";
            this.direccion.ReadOnly = true;
            // 
            // calle
            // 
            this.calle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.calle.DataPropertyName = "calle";
            this.calle.HeaderText = "Calle";
            this.calle.Name = "calle";
            this.calle.ReadOnly = true;
            // 
            // fechNacimiento
            // 
            this.fechNacimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fechNacimiento.DataPropertyName = "fechNacimiento";
            this.fechNacimiento.HeaderText = "Fecha Nacimiento";
            this.fechNacimiento.Name = "fechNacimiento";
            this.fechNacimiento.ReadOnly = true;
            // 
            // alergias_enfermedades
            // 
            this.alergias_enfermedades.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.alergias_enfermedades.DataPropertyName = "alergias_enfermedades";
            this.alergias_enfermedades.HeaderText = "Enfermedades";
            this.alergias_enfermedades.Name = "alergias_enfermedades";
            this.alergias_enfermedades.ReadOnly = true;
            // 
            // estatus
            // 
            this.estatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.estatus.DataPropertyName = "estatus";
            this.estatus.HeaderText = "Estado";
            this.estatus.Name = "estatus";
            this.estatus.ReadOnly = true;
            // 
            // estatus_descripcion
            // 
            this.estatus_descripcion.DataPropertyName = "estatus_descripcion";
            this.estatus_descripcion.HeaderText = "Descripcion de estado";
            this.estatus_descripcion.Name = "estatus_descripcion";
            this.estatus_descripcion.ReadOnly = true;
            // 
            // anioInicio
            // 
            this.anioInicio.DataPropertyName = "anioInicio";
            this.anioInicio.HeaderText = "Anio Inicio";
            this.anioInicio.Name = "anioInicio";
            this.anioInicio.ReadOnly = true;
            // 
            // anioGraducion
            // 
            this.anioGraducion.DataPropertyName = "anioGraducion";
            this.anioGraducion.HeaderText = "Anio Graduacion";
            this.anioGraducion.Name = "anioGraducion";
            this.anioGraducion.ReadOnly = true;
            // 
            // GestionAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 261);
            this.Controls.Add(this.dtgvAlumnos);
            this.Controls.Add(this.toolStrip1);
            this.Name = "GestionAlumnos";
            this.Text = "GestionAlumnos";
            this.Load += new System.EventHandler(this.GestionAlumnos_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox txtFiltro;
        private System.Windows.Forms.ToolStripButton toolEliminar;
        private System.Windows.Forms.DataGridView dtgvAlumnos;
        private System.Windows.Forms.ToolStripButton toolEditar;
        private System.Windows.Forms.ToolStripButton toolAgregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn NIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Papellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sapellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDepartamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn idMunicipio;
        private System.Windows.Forms.DataGridViewTextBoxColumn zonaUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn calle;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechNacimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn alergias_enfermedades;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatus_descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn anioInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn anioGraducion;
    }
}