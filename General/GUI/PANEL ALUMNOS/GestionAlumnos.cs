﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_ALUMNOS
{
    public partial class GestionAlumnos : Form
    {
        BindingSource _Alumnos = new BindingSource();
   

        private static void VerificarAlumno(String NIE)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.NIE, a.PNombre  FROM alumnos a WHERE NOT EXISTS(SELECT b.idAlumno FROM matriculas b WHERE a.NIE =" + NIE + ") ");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }

            catch { }
           // verificado = false;
        }

        public void CargarDatos()
        {
            try
            {
                _Alumnos.DataSource = CacheManager.SystemCache.TODOSLOSALUMNOS();
                FiltrarLocalmente();
                this.dtgvAlumnos.DataSource = _Alumnos;
            }
            catch(Exception err) { }
        }
        public void FiltrarLocalmente()
        {
            try
            {
                if (txtFiltro.TextLength > 0)
                {
                    _Alumnos.Filter = "PNombre LIKE '%" + txtFiltro.Text + "%' OR Papellido like '%" + txtFiltro.Text + "%'";
                }
                else
                {
                    _Alumnos.RemoveFilter();
                }
                dtgvAlumnos.AutoGenerateColumns = false;
                dtgvAlumnos.DataSource = _Alumnos;
                //lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registro encontrados";
            }
            catch
            {

            }
        }

        public GestionAlumnos()
        {
            InitializeComponent();
        }

        private void toolAgregar_Click(object sender, EventArgs e)
        {
            EdicionAlumnos frm = new EdicionAlumnos();
            frm.ShowDialog();

        }

        private void GestionAlumnos_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void toolEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea EDITAR el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                EdicionAlumnos frm = new EdicionAlumnos();

                frm.txtNIE.Text = dtgvAlumnos.CurrentRow.Cells["NIE"].Value.ToString();
                frm.txtPrimerNombre.Text = dtgvAlumnos.CurrentRow.Cells["Pnombre"].Value.ToString();
                frm.txtSegundoNombre.Text = dtgvAlumnos.CurrentRow.Cells["Snombre"].Value.ToString();
                frm.txtPrimerApellido.Text = dtgvAlumnos.CurrentRow.Cells["Papellido"].Value.ToString();
                frm.txtSegundoApellido.Text = dtgvAlumnos.CurrentRow.Cells["Sapellido"].Value.ToString();
               // frm.cmbDepartamento.Text = dtgvAlumnos.CurrentRow.Cells["idDepartamento"].Value.ToString();
                //frm.cmbMunicipio.Text = dtgvAlumnos.CurrentRow.Cells["idMunicipio"].Value.ToString();
                frm.cmbZona.Text = dtgvAlumnos.CurrentRow.Cells["zonaUR"].Value.ToString();
                frm.txtDireccion.Text = dtgvAlumnos.CurrentRow.Cells["direccion"].Value.ToString();
                frm.cmbCalle.Text = dtgvAlumnos.CurrentRow.Cells["calle"].Value.ToString();
                frm.dtpFecha.Text = dtgvAlumnos.CurrentRow.Cells["fechNacimiento"].Value.ToString();
                frm.txtEnfermedades.Text = dtgvAlumnos.CurrentRow.Cells["alergias_enfermedades"].Value.ToString();
                //frm.rbtEstado.Text = dtgvAlumnos.CurrentRow.Cells["estatus"].Value.ToString();
                frm.txtDescripcion.Text = dtgvAlumnos.CurrentRow.Cells["estatus_descripcion"].Value.ToString();
                frm.dtpInicio.Text = dtgvAlumnos.CurrentRow.Cells["anioInicio"].Value.ToString();
                //frm.cmbMun
                frm.ShowDialog();
                CargarDatos();
            }
        }

        private void toolEliminar_Click(object sender, EventArgs e)
        {
 

        }
    }
}
