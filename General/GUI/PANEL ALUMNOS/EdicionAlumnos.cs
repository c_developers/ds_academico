﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI.PANEL_ALUMNOS
{
    public partial class EdicionAlumnos : Form
    {
        int estado;
        //String graduacion = "2019-05-05";


        private int ValidarRadio()
        {
            int radio;
            if (this.rbtEstado.Checked)
            {
                radio = 1;
            }
            else
            {
                radio = 0;
            }
            return radio;
        }

       private void Departamentos()
        {
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            String query = "SELECT idDepartamento,departamento FROM departamentos";
            DataTable dt = oOperacion.Consultar(query);
            try
            {
                this.cmbDepartamento.DataSource = dt;
                this.cmbDepartamento.DisplayMember = "departamento";
                this.cmbDepartamento.ValueMember = "idDepartamento";
                this.cmbDepartamento.SelectedIndex = 0;

                /*for(int i=0; i< dt.Rows.Count; i++)
                {
                    this.cmbDepartamento.Items.Add(dt.Rows[i]["departamento"]);
                }*/
            }
            catch
            {

            }
        }
        private void Municipios(String m)
        {
           // this.cmbMunicipio.Items.Clear();
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            String query = "SELECT a.idMunicipio, a.municipio FROM municipios a, departamentos b where a.idDepartamento=b.idDepartamento and  b.departamento='" + m + "'";
            DataTable dtmuni = oOperacion.Consultar(query);
            try
            {
                cmbMunicipio.DataSource = dtmuni;
                cmbMunicipio.DisplayMember = "municipio";
                cmbMunicipio.ValueMember = "idMunicipio";
                cmbMunicipio.SelectedIndex = 0;
                /*

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    this.cmbMunicipio.Items.Add(dt.Rows[i]["municipio"]);
                }*/
            }
            catch
            {

            }

        }
        public void Procesar()
        {
            CLS.Alumnos alumno = new CLS.Alumnos();
            alumno.IDAlumno = txtNIE.Text;
            alumno.PrimerNombre = txtPrimerNombre.Text;
            alumno.SegundoNombre = txtSegundoNombre.Text;
            alumno.PrimerApellido = txtPrimerApellido.Text;
            alumno.SegundoApellido = txtSegundoApellido.Text;
            alumno.IdDepartamento = cmbDepartamento.SelectedValue.ToString();
            alumno.IdMunicipio = cmbMunicipio.SelectedValue.ToString();
            alumno.Zona = cmbZona.SelectedItem.ToString();
            alumno.Direccion = txtDireccion.Text;
            alumno.Calle = cmbCalle.SelectedItem.ToString();
            alumno.FechaNacimiento = dtpFecha.Text;
            alumno.AlergiasEnfermedades = txtEnfermedades.Text;
            alumno.Estatus = ValidarRadio();
            alumno.EstatusDescripcion = txtDescripcion.Text;
            alumno.AnioInicio = dtpInicio.Text;
            //alumno.AnioGraduacion = graduacion;

            if (txtNIE.TextLength != 0)
            {
                //Estoy insertando un nuevo regitro
                if (alumno.Guardar())
                {
                    MessageBox.Show("Registro guardado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser guardado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                //Estoy actualizando un registro
                if (alumno.Actualizar())
                {
                    MessageBox.Show("Registro actualizado correctamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show("El registro no pudo ser actualizado correctamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private Boolean Verificacion()
        {
            Boolean Verificado = true;
            Notificador.Clear();
            if (txtPrimerNombre.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtPrimerNombre, "Este campo no puede quedar vacío");
            }

            if (txtSegundoNombre.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtSegundoNombre, "Este campo no puede quedar vacío");
            }

            if (txtPrimerApellido.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtPrimerApellido, "Este campo no puede quedar vacío");
            }


            if (txtSegundoApellido.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txtSegundoApellido, "Este campo no puede quedar vacío");
            }

            if (cmbDepartamento.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cmbDepartamento, "Este campo no puede quedar vacío");
            }
            if (cmbMunicipio.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cmbMunicipio, "Este campo no puede quedar vacío");
            }
            return Verificado;
        }
        public EdicionAlumnos()
        {
            InitializeComponent();
            Departamentos();
        }

      

        private void btnCargar_Click(object sender, EventArgs e)
        {
            Municipios(this.cmbDepartamento.Text);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void rbtEstado_Click(object sender, EventArgs e)
        {
            if (rbtEstado.Checked)
            {
                rbtEstado.Checked = true;
            }
            else
            {
                rbtEstado.Checked = false;
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            DEPARTAMENTOS_Y_MUNICIPIOS.EditarDepaMuni frm = new DEPARTAMENTOS_Y_MUNICIPIOS.EditarDepaMuni();
            frm.ShowDialog();
            Departamentos();
           
        }
    }
}
