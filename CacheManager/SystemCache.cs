﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CacheManager
{
    public static class SystemCache
    {

        public static DataTable ValidarUsuario(String pUsuario, String pCredencial)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT a.idUsuarios, a.usuario, a.contrasenia, a.idRol, b.nombreRol FROM usuarios a, rolusuarios b WHERE a.idRol = b.idRol
            AND a.usuario='" + pUsuario + @"'
            AND a.contrasenia=sha1('" + pCredencial + "');");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }


        public static DataTable TODOSLOSEMPLEADOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT IDEmpleado,
            Nombres, Apellidos,
            FechaNacimiento, DUI,
            NIT, Telefono,
            Direccion from empleados
            order by Apellidos, Nombres;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSUSUARIOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select a.IDUsuario, a.Usuario, CONCAT(b.Nombres,' ',b.Apellidos) 
            as 'Empleado', c.Rol, a.Estado, a.IDEmpleado, a.IDRol from Usuarios a, Empleados b, Roles c 
            where a.IDEmpleado=b.IDEmpleado and a.IDRol=c.IDRol;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }

        public static DataTable TODOSLOSROLES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"Select a.idRol, a.nombreRol  from rolusuarios a order by a.idRol asc");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }
        public static DataTable PERMISOSDEUNROL(String IDRol)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT IDOpcion, Opcion, Clasificacion, 
            (SELECT IDPermiso FROM permisos z WHERE z.IDRol =" + IDRol + @" AND Z.IDOpcion = a.IDOpcion) AS 'IDPermiso',
            IFNULL((SELECT '1' FROM permisos z WHERE z.IDRol =" + IDRol + @" AND Z.IDOpcion = a.IDOpcion),'0') AS 'Asignado' FROM opciones a ");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable PermisosUsuario(String pUsuario)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select d.IDOpcion, d.opcion from usuarios a, permisos c, 
            opciones d where a.Usuario='" + pUsuario + @"' and
            a.IDRol = c.IDRol and c.IDOpcion = d.IDOpcion;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }
        public static bool vacio; // Variable utilizada para saber si hay algún TextBox vacio.
        public static void validar(Form formulario)
        {
            foreach (Control oControls in formulario.Controls) // Buscamos en cada TextBox de nuestro Formulario.
            {
                if (oControls is TextBox & oControls.Text == String.Empty) // Verificamos que no este vacio.
                {
                    vacio = true; // Si esta vacio el TextBox asignamos el valor True a nuestra variable.
                }
            }
            if (vacio == true) MessageBox.Show("Favor de llenar todos los campos."); // Si nuestra variable es verdadera mostramos un mensaje.
            vacio = false; // Devolvemos el valor original a nuestra variable.
        }
        //Funcion para validar el enter.
        public static Boolean Enter(KeyPressEventArgs evento)
        {
            bool re = false;
            if ((int)evento.KeyChar == (int)Keys.Enter)
            {
                re = true;
            }
            return re;
        }
        //Consultar Alumnos segun NIE o primer y segundo nombre.
        public static DataTable Alumnos(String NIE)
        {
            Convert.ToInt32(NIE);
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
           Sentencia.Append(@"SELECT a.NIE, a.PNombre, a.SNombre, a.Papellido, a.Sapellido, a.zonaUR, a.fechNacimiento, a.anioInicio, a.anioGraducion, a.estatus, a.direccion FROM alumnos a  WHERE NIE='" + NIE + @"'");
                try
                {
                    Resultado = oConsulta.Consultar(Sentencia.ToString());
                }
                catch
                {
                    Resultado = new DataTable();
                }
            return Resultado;
        }

        public static DataTable AlumnosNombre(String n)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
                Sentencia.Append(@"SELECT a.NIE, a.PNombre, a.SNombre, a.Papellido, a.Sapellido, a.zonaUR, a.fechNacimiento, a.anioInicio, a.anioGraducion, a.estatus, a.direccion FROM alumnos a  WHERE concat(PNombre,' ',PApellido) ='" + n + @"'");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable Docentes()
        {
           // Convert.ToInt32(NIP);
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idDocente, a.NIDocente, a.NUP, a.nombre, a.apellido, a.DUI, a.NIT, a.telefono, a.direccion, a.laborando, b.nombreCarrera
                            FROM Docentes a, titulos b WHERE b.idTitulo = a.idTitulo AND  a.NIDocente = 4");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;
        }
        public static DataTable TODOSLOSALUMNOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion alumnos = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.NIE, a.PNombre, a.SNombre, a.Papellido, a.Sapellido ,a.idDepartamento, a.idMunicipio, a.zonaUR, a.direccion, a.calle, a.fechNacimiento, a.alergias_enfermedades, a.estatus,a.estatus_descripcion, a.anioInicio, a.anioGraducion FROM alumnos a");
            try
            {
                Resultado = alumnos.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }
            return Resultado;

        }
        public static DataTable TODOSLOSENCARGADOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idEncargado, a.nombres_encargado, a.apellidos_encargado, a.telefono, a.DUI, a.trabajo FROM encargados a");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch(Exception err) { }
            return Resultado;
        }
        
        public static DataTable DocentesUsuario()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idDocente, CONCAT(a.nombre, ' ', a.apellido) AS 'Nombre' FROM docentes a");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }

        public static DataTable USUARIOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idUsuarios, a.usuario, a.contrasenia,a.idDocente, a.idRol, b.nombreRol FROM usuarios a, rolusuarios b
                               WHERE a.idRol = b.idRol;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable DEPA_MUNI()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idDepartamento, a.departamento,b.idMunicipio, b.municipio  FROM departamentos a, municipios b WHERE a.idDepartamento = b.idDepartamento;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable DEPARTAMENTOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idDepartamento, a.departamento  FROM departamentos a;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable TODOSLOSDOCENTES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idDocente, a.NIDocente, a.NUP, a.nombre, a.apellido, a.DUI, a.NIT, a.telefono, a.direccion, a.laborando, a.idTitulo, b.nombreCarrera
                              FROM  docentes a, titulos b WHERE b.idTitulo = a.idTitulo;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable Titulos()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idTitulo, a.nombreCarrera FROM titulos a;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable BACHILLERATOS() {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idBachillerato, a.nombreResultante, a.anioLectivo, a.TipoBachillerato, a.idEspecialidadBach, b.NombreEsp FROM bachilleratos a, 
                               especialidadbachillerato b WHERE b.idEspecialidadBach = a.idEspecialidadBach;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static DataTable ESPECIALIDADESBACHILLERATOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT a.idEspecialidadBach, a.NombreEsp FROM especialidadbachillerato a;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch (Exception err) { }
            return Resultado;
        }
        public static Boolean DatoExistente(String anio, String tipo, int especialidad)
        {
            Boolean aux = false;
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"SELECT idBachillerato FROM bachilleratos
                             where  anioLectivo='" + anio + "' AND TipoBachillerato='" + tipo + "' AND idEspecialidadBach=" + especialidad + ";");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
                aux = true;
            }
            catch (Exception err) { }
            return aux;
        }
        public static int BUSCARIDESPE(String SelectCombo)
        {
            String Consulta = "SELECT idEspecialidadBach from especialidadbachillerato where NombreEsp='" + SelectCombo + "' limit 1;";
            String aux = "";
            DataTable ID = new DataTable();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            try
            {
                ID = oConsulta.Consultar(Consulta);
                aux = ID.Rows[0].ToString();
            }catch(Exception err) { }
            int valor = Convert.ToInt32(aux);
            return valor;
        }
    }
  }
